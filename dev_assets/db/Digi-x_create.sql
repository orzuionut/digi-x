CREATE TABLE companies
(
    id INT(10) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    user_id INT(10) UNSIGNED NOT NULL,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    CONSTRAINT companies_user_id_foreign FOREIGN KEY (user_id) REFERENCES users (id)
);


CREATE TABLE jobs
(
    id INT(10) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    company_id INT(10) UNSIGNED NOT NULL,
    position_id INT(10) UNSIGNED NOT NULL,
    user_id INT(10) UNSIGNED NOT NULL,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    CONSTRAINT jobs_company_id_foreign FOREIGN KEY (company_id) REFERENCES companies (id),
    CONSTRAINT jobs_position_id_foreign FOREIGN KEY (position_id) REFERENCES positions (id),
    CONSTRAINT jobs_user_id_foreign FOREIGN KEY (user_id) REFERENCES users (id)
);

CREATE TABLE locations
(
    id INT(10) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    user_id INT(10) UNSIGNED NOT NULL,
    name VARCHAR(255),
    country VARCHAR(255),
    country_code VARCHAR(3),
    city VARCHAR(255),
    state VARCHAR(255),
    street VARCHAR(255),
    zip VARCHAR(255),
    latitude DECIMAL(9,6) NOT NULL,
    longitude DECIMAL(9,6) NOT NULL,
    CONSTRAINT locations_user_id_foreign FOREIGN KEY (user_id) REFERENCES users (id)
);

CREATE TABLE memories
(
    id INT(10) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    title VARCHAR(255) NOT NULL,
    file_name VARCHAR(255),
    source VARCHAR(255),
    fb_id BIGINT(20) UNSIGNED,
    description TEXT NOT NULL,
    trunk_id INT(10) UNSIGNED NOT NULL,
    user_id INT(10) UNSIGNED NOT NULL,
    type_id INT(10) UNSIGNED NOT NULL,
    location_id INT(10) UNSIGNED,
    job_id INT(10) UNSIGNED,
    memory_date DATE,
    meta_data BLOB,
    deleted_at TIMESTAMP,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    CONSTRAINT memories_job_id_foreign FOREIGN KEY (job_id) REFERENCES jobs (id),
    CONSTRAINT memories_location_id_foreign FOREIGN KEY (location_id) REFERENCES locations (id),
    CONSTRAINT memories_trunk_id_foreign FOREIGN KEY (trunk_id) REFERENCES trunks (id),
    CONSTRAINT memories_type_id_foreign FOREIGN KEY (type_id) REFERENCES memory_types (id),
    CONSTRAINT memories_user_id_foreign FOREIGN KEY (user_id) REFERENCES users (id)
);

CREATE TABLE memory_person
(
    memory_id INT(10) UNSIGNED NOT NULL,
    person_id INT(10) UNSIGNED NOT NULL,
    CONSTRAINT `PRIMARY` PRIMARY KEY (memory_id, person_id),
    CONSTRAINT memory_person_memory_id_foreign FOREIGN KEY (memory_id) REFERENCES memories (id),
    CONSTRAINT memory_person_person_id_foreign FOREIGN KEY (person_id) REFERENCES persons (id)
);

CREATE TABLE memory_types
(
    id INT(10) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL
);

CREATE TABLE persons
(
    id INT(10) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL,
    date_of_birth DATE,
    relationship_id INT(10) UNSIGNED NOT NULL,
    user_id INT(10) UNSIGNED NOT NULL,
    facebook_id INT(10) UNSIGNED,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    CONSTRAINT persons_relationship_id_foreign FOREIGN KEY (relationship_id) REFERENCES relationship_types (id),
    CONSTRAINT persons_user_id_foreign FOREIGN KEY (user_id) REFERENCES users (id)
);

CREATE TABLE positions
(
    id INT(10) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    title VARCHAR(255) NOT NULL,
    user_id INT(10) UNSIGNED NOT NULL,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    CONSTRAINT positions_user_id_foreign FOREIGN KEY (user_id) REFERENCES users (id)
);

CREATE TABLE relationship_types
(
    id INT(10) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    degree INT(10) UNSIGNED NOT NULL
);

CREATE TABLE sharelinks
(
    id INT(10) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    memory_id INT(10) UNSIGNED NOT NULL,
    user_id INT(10) UNSIGNED NOT NULL,
    hashed_link BLOB NOT NULL,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    CONSTRAINT sharelinks_memory_id_foreign FOREIGN KEY (memory_id) REFERENCES memories (id),
    CONSTRAINT sharelinks_user_id_foreign FOREIGN KEY (user_id) REFERENCES users (id)
);

CREATE TABLE trunks
(
    id INT(10) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    title VARCHAR(255) NOT NULL,
    description VARCHAR(255) NOT NULL,
    user_id INT(10) UNSIGNED NOT NULL,
    meta_data BLOB,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    CONSTRAINT trunks_user_id_foreign FOREIGN KEY (user_id) REFERENCES users (id)
);

CREATE TABLE users
(
    id INT(10) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    password VARCHAR(60) NOT NULL,
    remember_token VARCHAR(100),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);
