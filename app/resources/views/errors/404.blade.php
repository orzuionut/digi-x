@extends('layouts.master')

@section('title', '404 - Not found')

@section('content')
    <div class="section-404">
        <div class="container-sm">
            <div class="text">
                <h1>Oops!</h1>
                <h3> We can't seem to find the page you're looking for.</h3>
                <a href="javascript:history.go(-1)" class="button">Take me back</a>
            </div>
            <div class="gif">
                <img src="{{ URL::asset('img/404.gif') }}" width="313" height="428" class="hide-sm" alt="404 page.">
            </div>
        </div>
    </div>
@endsection