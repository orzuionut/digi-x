<header>
    <div class="container">
        <div class="logo">
            <h1><a href="{{ url('/')  }}">Digi-X</a></h1>
        </div>
        <div class="menu">
            <ul>
                @if(!Auth::check())
                    <li><a href="{{ url('login') }}">Login</a></li>
                    <li><a href="{{ url('register') }}">Register</a></li>
                @else
                    <li><a href="{{ route('memories.index') }}">Memories</a></li>
                    <li><a href="{{ route('trunks.index') }}">Trunks</a></li>
                    <li><a href="{{ route('jobs.index') }}">Jobs</a></li>
                    <li><a href="{{ route('persons.index') }}">Persons</a></li>
                    <li><a href="{{ route('sharelinks.index') }}">Sharelinks</a></li>
                    <li><a href="{{ route('profile.edit') }}">Profile</a></li>
                    <li><a href="{{ url('logout') }}">Logout</a></li>
                @endif
            </ul>
        </div>
    </div>
</header>