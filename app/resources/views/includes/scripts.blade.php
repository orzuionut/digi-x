<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>


<!-- formstone -->
<script type='text/javascript' src='{{ URL::asset('js/vendor/formstone/core.js') }}'></script>
<script type='text/javascript' src='{{ URL::asset('js/vendor/formstone/checkbox.js') }}'></script>
<script type='text/javascript' src='{{ URL::asset('js/vendor/formstone/scrollbar.js') }}'></script>
<script type='text/javascript' src='{{ URL::asset('js/vendor/formstone/touch.js') }}'></script>
<script type='text/javascript' src='{{ URL::asset('js/vendor/formstone/dropdown.js') }}'></script>



<!-- video js -->
<script src="http://vjs.zencdn.net/5.10.2/video.js"></script>


<!-- modal -->
<script type='text/javascript' src='{{ URL::asset('js/vendor/bootstrap.js') }}'></script>
<!-- datepicker -->
<script type='text/javascript' src='{{ URL::asset('js/vendor/moment.js') }}'></script>
<script type='text/javascript' src='{{ URL::asset('js/vendor/bootstrap-datetimepicker.js') }}'></script>
<!-- selectize -->
<script type='text/javascript' src='{{ URL::asset('js/vendor/selectize.js') }}'></script>

<!-- utils -->
<script type='text/javascript' src='{{ URL::asset('js/jquery.digix-utils.js') }}'></script>


<!-- location field -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script type='text/javascript' src='{{ URL::asset('js/vendor/typehead.bundle.js') }}'></script>
<script type='text/javascript' src='{{ URL::asset('js/jquery.digix-location.js') }}'></script>

<!-- memories -->
<script type='text/javascript' src='{{ URL::asset('js/pages/memories.js') }}'></script>

<!-- memories.index -->
<script type='text/javascript' src='{{ URL::asset('js/vendor/jquery.mixitup.min.js') }}'></script>
<script type='text/javascript' src='{{ URL::asset('js/jquery.digix-filters.js') }}'></script>

<!-- memories.create -->
<script type='text/javascript' src='{{ URL::asset('js/vendor/plupload/plupload.full.min.js') }}'></script>
<!-- debug
<script type="text/javascript" src="../js/moxie.js"></script>
<script type="text/javascript" src="../js/plupload.dev.js"></script>
-->
<script type='text/javascript' src='{{ URL::asset('js/jquery.digix-upload.js') }}'></script>


<!-- jobs.add -->
<script type='text/javascript' src='{{ URL::asset('js/pages/jobs.js') }}'></script>
<!-- persons.add -->
<script type='text/javascript' src='{{ URL::asset('js/pages/persons.js') }}'></script>

<script type='text/javascript' src='{{ URL::asset('js/jquery.script.js') }}'></script>

<!-- copy to clipboard -->
<script type='text/javascript' src='{{ URL::asset('js/vendor/clipboard.min.js') }}'></script>

<script type='text/javascript' src='{{ URL::asset('js/jquery.tooltipster.min.js') }}'></script>
<script type='text/javascript' src='{{ URL::asset('js/js.clipboard.js') }}'></script>