<footer>
    <p class="copyright">Copyright © 2016 - Orzu Ionut, Bunea Daniel, Calara Cristian</p>
</footer>
@if (Session::has('flash_message'))
    <div class="flash-notification {{ Session::get('flash_message_type') }}">
        <div class="container">
            <div class="messages">
                <span>{{ Session::get('flash_message') }}</span>
            </div>
            <button type="button" class="close" ><span aria-hidden="true">×</span></button>
        </div>
    </div>
@else
    <div class="flash-notification">
        <div class="container">
            <div class="messages"></div>
            <button type="button" class="close" ><span aria-hidden="true">×</span></button>
        </div>
    </div>
@endif
