<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Digi-X - @yield('title')</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href='{{ URL::asset('css/vendor/formstone/checkbox.css') }}' rel='stylesheet' type='text/css'>
<link href='{{ URL::asset('css/vendor/formstone/scrollbar.css') }}' rel='stylesheet' type='text/css'>
<link href='{{ URL::asset('css/vendor/formstone/dropdown.css') }}' rel='stylesheet' type='text/css'>
<link href="http://vjs.zencdn.net/5.10.2/video-js.css" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="{{ URL::asset('css/vendor/font-awesome/css/font-awesome.min.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('css/tooltipster.css') }}" />