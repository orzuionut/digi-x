@extends('layouts.master')
@section('title', 'Memories')
@section('body-class', 'memories-list-template')

@section('content')
    <div class="page-title with-image">
        <div class="overlay"></div>
        <h1>Your memories</h1>
        <a href="{{ route('memories.create') }}" class="button add">
            <i class="fa fa-plus"></i>
        </a>
    </div>
    <main class="main-content">
        <div class="tab-filter-wrapper action-bar">
            <div class="tab-filter">
                <ul class="memory-type-filters filters">
                    <li class="placeholder">
                        <a data-type="all" href="#0">All</a> <!-- selected option on mobile -->
                    </li>
                    <li class="filter" data-filter="all"><a class="selected" href="#0" data-type="all">All</a></li>
                    @foreach($memory_types as $type)
                        <li class="filter" data-filter=".type-{{ $type->name }}"><a href="#0" data-type="{{ $type->name }}">{{ $type->name }}</a></li>
                    @endforeach
                </ul> <!-- filters -->
            </div> <!-- tab-filter -->
        </div>
        <section class="memories">
            <ul class="grid">
                @foreach($memories as $memory)
                <li class="col mix type-{{ $memory->type->name }}" data-id="{{ $memory->id }}">
                    <div class="box @if($memory->type->name != 'image') no-image-type @endif">
                        <div class="image" @if($memory->type->name == 'image') style="background-image: url('@if($memory->file_name){{ url('memoryfile/' . $memory->id ) }}@else {{ $memory->source }} @endif')" @endif>
                            <i class="fa fa-icon {{ $memory->type->class }}"></i>
                        </div>
                        <div class="info">
                            <h3><a href="{{ route('memories.show', $memory->id) }}" >{{ str_limit($memory->title, $limit = 17, $end = '...') }}</a></h3>
                            <div class="content">
                                <p>{{ str_limit($memory->description, $limit = 55, $end = '...') }}</p>
                            </div>
                            <div class="meta">
                                <span class="date">{{ $memory->memory_date->format('Y/m/d') }}</span>
                                <span class="trunk">{{ $memory->trunk->title }}</span>
                            </div>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
            <div class="fail-message">No results found</div>
        </section> <!-- gallery -->
        <div class="filter-cont">
            <form data-ids="{{ $memories_ids_json }}">
                <div class="filter-block form-field">
                    <h4>Search</h4>
                    <div class="filter-content">
                        <input type="search" name="title_and_content">
                    </div>
                </div>
                <div class="filter-block form-field">
                    <h4>Trunk</h4>
                    <div class="filter-content">
                        <div class="select filters">
                            <select name="trunk_id">
                                <option value="">Choose an option</option>
                                @foreach($trunks as $trunk)
                                    <option value="{{ $trunk->id }}">{{ $trunk->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="filter-block form-field">
                    <h4>Persons</h4>
                    <div class="filter-content">
                        <div class="select filters">
                            <select name="persons[]" class="selectize" multiple>
                                @foreach($persons as $person)
                                    <option value="{{ $person->id }}">{{ $person->display_name() }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="filter-block form-field">
                    <h4>Relationships</h4>
                    <div class="filter-content">
                        <div class="select filters">
                            <select name="relationships_names[]" class="selectize" multiple>
                                @foreach($relationships as $relationship)
                                    <option value="{{ $relationship->name }}">{{ $relationship->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="filter-block form-field">
                    <h4>Relationships degree</h4>
                    <div class="filter-content">
                        <div class="select filters">
                            <select name="relationships_degrees[]" class="selectize" multiple>
                                @foreach($relationships as $relationship)
                                    <option value="{{ $relationship->degree }}">{{ $relationship->degree }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="filter-block form-field">
                    <h4>Location</h4>
                    <div class="filter-content">
                        <div class="select filters">
                            <select name="location_id">
                                <option value="">Choose an option</option>
                                @foreach($locations as $location)
                                    <option value="{{ $location->id }}">{{ $location->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="filter-block form-field">
                    <h4>Companies</h4>
                    <ul class="filter-content filters list">
                        @foreach($companies as $company)
                        <li>
                            <input class="filter" name="companies[]" type="checkbox" id="company{{ $company->id }}" value="{{ $company->id }}">
                            <label class="checkbox-label" for="company{{ $company->id }}">{{ $company->name }}</label>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div class="filter-block form-field">
                    <h4>Positions</h4>
                    <ul class="filter-content filters list">
                        @foreach($positions as $position)
                            <li>
                                <input type="checkbox" name="positions[]" id="position{{ $position->id }}" value="{{ $position->id }}">
                                <label class="checkbox-label" for="position{{ $position->id }}">{{ $position->title }}</label>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="filter-block form-field">
                    <h4>Date</h4>
                    <div class="filter-content">
                        <input type="text" name="date_range_or_single" placeholder="Jan 1998 to Dec 2016">
                    </div>
                </div>
                <input type="submit" class="button full-width" value="Update">
            </form>
            <a href="#0" class="close">Close</a>
        </div> <!-- filter -->
        <a href="#0" class="filter-trigger">Filters</a>
    </main>
@endsection