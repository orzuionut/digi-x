@extends ('layouts.master')

@section('title', 'Memory - Digi-X')
@section('body-class', 'memories-template  show-memory-template')

@section('content')
    <div class="memory-file-cont type-{{ $memory->type->name }}">
        <div class="overlay"></div>
        <div class="vertical-align-helper"></div>
        @if($memory->type->name == 'image')
        <img class="memory-file"
             src="@if($memory->file_name){{url('memoryfile/' . $memory->id ) }}@else{{ $memory->source }}@endif"
             alt="#">
        @elseif($memory->type->name == 'video')
            <div class="video-wrapper">
                <video class="video-js vjs-default-skin vjs-big-play-centered memory-file" controls
                       preload="auto"
                       data-setup='{}'>
                    <source src="@if($memory->file_name){{url('memoryfile/' . $memory->id ) }}@else{{ $memory->source }}@endif" type="video/{{ $memory->ext() }}">
                    <p class="vjs-no-js">
                        To view this video please enable JavaScript, and consider upgrading to a web browser
                        that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                    </p>
                </video>
            </div>
        @else
            <a class="memory-file icon" href="@if($memory->file_name){{url('memoryfile/' . $memory->id ) }}@else{{ $memory->source }}@endif" target="_blank">
                <i class="fa fa-cloud-download"></i>
            </a>
        @endif
    </div>
    <div class="show-memory section form-section">
        <div class="container">
            <div class="title">
                <h2>{{ $memory->title }}</h2>
            </div>
            <span class="date">
                <i class="fa fa-calendar-o"></i>
                {{ $memory->memory_date->diffForHumans() }}
            </span>
            <div class="content">
                <p>
                    {{ $memory->description }} -
                    @if ($memory->location)
                        <span><b>at</b> {{ $memory->location->name }}</span>
                    @endif
                    @if (!$memory->persons->isEmpty())
                        <span><b>with</b>
                            @foreach($memory->persons as $key => $person)
                                @if($key != 0),@endif
                                <a href="{{ route('persons.edit', $person->id) }}">{{ $person->display_name() }}</a>
                            @endforeach
                        </span>
                    @endif
                    @if ($memory->trunk)
                        <span><b>in</b> <a href="{{ route('trunks.edit', $memory->trunk->id) }}">{{ $memory->trunk->title }}</a></span>
                    @endif
                </p>
            </div>
            <div class="actions">
                <a href="{{ route('memories.edit', $memory->id) }}" class="button">Edit memory</a>
                <a href="{{ route('memories.destroy', $memory->id) }}" class="button delete-memory" data-id="{{ $memory->id }}" data-token="{{ csrf_token() }}">Delete memory</a>
            </div>
        </div>
    </div>
@endsection