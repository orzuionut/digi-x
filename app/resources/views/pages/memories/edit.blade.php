    @extends ('layouts.master')

@section('title', 'Edit memory - Digi-X')
@section('body-class', 'memories-template  edit-memory-template')

@section('content')
    <div class="page-title with-image">
        <div class="overlay"></div>
        <h1>Edit memory</h1>
    </div>
    <div class="edit-memory section form-section">
        <div class="container">
            <form method="POST" action="{{ action('MemoryController@update', $memory->id) }}">
                <input type="hidden" name="_method" value="PUT">
                {!! csrf_field() !!}
                <div class="modal-body">
                    <div class="form-field">
                        <label>Title</label>
                        <input type="text" name="title" value="{{ $memory->title }}">
                        {!! d_error('title', $errors->toArray()) !!}
                    </div>
                    <div class="form-field">
                        <label>Description</label>
                        <textarea name="description">{{ $memory->description }}</textarea>
                        {!! d_error('description', $errors->toArray()) !!}
                    </div>
                    <div class="form-field">
                        <label>Trunk</label>
                        <select name="trunk_id">
                            @foreach($trunks as $trunk)
                                <option value="{{ $trunk->id }}" @if($trunk->id == $memory->trunk_id) selected @endif>{{ $trunk->title }}</option>
                            @endforeach
                        </select>
                        {!! d_error('trunk_id', $errors->toArray()) !!}
                    </div>
                    <div class="form-field">
                        <label>Persons in memory</label>
                        <select name="persons[]" class="selectize" multiple>
                            @foreach($persons as $person)
                                <option value="{{ $person->id }}" @if($memory->persons->contains($person->id)) selected @endif>{{ $person->display_name() }}</option>
                            @endforeach
                        </select>
                        {!! d_error('persons', $errors->toArray()) !!}
                    </div>
                    <div class="form-field">
                        <label for="password">Location</label>
                        <input class="location" name="location_name" type="text" value="{{ $memory->location->name }}">
                        <input type="hidden" name="location_data" value="">
                        {!! d_error('location', $errors->toArray()) !!}
                    </div>
                    <div class="form-field">
                        <label>Job</label>
                        <select name="job_id">
                            <option value="0">No job</option>
                            @foreach($jobs as $job)
                                <option value="{{ $job->id }}"  @if($job->id == $memory->job_id) selected @endif>{{ $job->position->title }}, {{ $job->company->name }}</option>
                            @endforeach
                        </select>
                        {!! d_error('job_id', $errors->toArray()) !!}
                    </div>
                    <div class="form-field">
                        <label>Date</label>
                        <input name="memory_date" type='text' class="datepicker" value="{{ $memory->memory_date->format('d/m/Y') }}"/>
                        {!! d_error('memory_date', $errors->toArray()) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="full-width">Save changes</button>
                </div>
            </form>
        </div>
    </div>
@endsection