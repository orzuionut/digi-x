@extends ('layouts.master')

@section('title', 'Add memory - Digi-X')
@section('body-class', 'memories-template create-memory-template')

@section('content')
    <div class="page-title with-image">
        <div class="overlay"></div>
        <h1>Add memory</h1>
    </div>
    <div class="action-bar">
        <div class="container">
            <div class="trunk">
                {{--<label>Trunk:</label>--}}
                {{--<div class="tooltip-select">--}}
                    {{--<select name="trunk">--}}
                        {{--<option>Personal</option>--}}
                        {{--<option>Work</option>--}}
                        {{--<option>Work</option>--}}
                        {{--<option>Work</option>--}}
                        {{--<option>Work</option>--}}
                        {{--<option>Work</option>--}}
                        {{--<option>Work</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
            </div>
            <div class="social">
                <span>Add with:</span>
                <ul>
                    <li><a href="#" data-toggle="modal" data-target="#import-facebook-memory">Facebook</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#import-instagram-memory">Instagram</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="drag-drop-section">
        <div class="container">
            <p class="intro">You can add <a href="{{ url('persons/create') }}">persons</a>, <a href="{{ url('jobs/create') }}">jobs</a> and <a href="{{ url('trunks/create') }}">trunks</a> for a better use of the filters. Allowed filetypes: png, jpeg, jpg, mp4, qt, mov, avi, txt, pdf, doc, docx, ppt, pptx, xls, xml, rtf, mp3, wav, midi. </p>

            <div id="drag-drop-area" style="position: relative;">
                <div class="drag-drop-inside">
                    <div class="cloud-upload-container">
                        <i id="plupload-browse-button" class="fa fa-icon fa-cloud-upload "></i>
                    </div>
                </div>
            </div>
            <div id="files-list">
                @if($memories)
                    @foreach($memories as $memory)
                        <div class="file" id="{{ $memory->social_html_id }}" data-prepopulate="{{ json_encode($memory) }}">
                            <span class="type">{{ $memory->type_name }}</span>
                            <span class="name">{{ $memory->title }}</span>
                            <span class="size">
                                @if($memory->social_type == 'facebook')
                                    <i class="fa fa-facebook-official"></i>
                                @elseif($memory->social_type == 'instagram')
                                    <i class="fa fa-instagram"></i>
                                @endif
                                @if($memory->notes)<b>{{ $memory->notes }}</b>@endif
                            </span>
                            <span class="status">
                                <span class="message">
                                    <a href="#" class="upload-by-url">Upload</a>
                                    <a href="{{ $memory->source }}" target="_blank" class="see-asset">See</a>
                                    <a href="#" class="remove">Remove</a>
                                </span>
                                <div class="progress-bar"><div class="helper"></div></div>
                            </span>
                        </div>
                    @endforeach
                @endif
            </div>
            <!--a id="upload-files" class="button" href="#">Upload memories</a-->
        </div>
    </div>
    <div class="modal fade" id="edit-memory" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h2>Quick edit</h2>
                </div>
                <form method="POST" action="{{ action('MemoryController@store') }}">
                    {!! csrf_field() !!}
                    <div class="modal-body">
                        <div class="form-field">
                            <label>Title</label>
                            <input type="text" name="title" value="">
                        </div>
                        <div class="form-field">
                            <label>Description</label>
                            <textarea name="description"></textarea>
                        </div>
                        <div class="form-field">
                            <label>Trunk</label>
                            <select name="trunk_id">
                                @foreach($trunks as $trunk)
                                    <option value="{{ $trunk->id }}">{{ $trunk->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-field">
                            <label>Persons in memory</label>
                            <select name="persons[]" class="selectize" multiple>
                                @foreach($persons as $person)
                                    <option value="{{ $person->id }}">{{ $person->display_name() }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-field">
                            <label>Location</label>
                            <input class="location" name="location_name" type="text" value="">
                            <input type="hidden" name="location_data" value="">
                        </div>
                        <div class="form-field">
                            <label>Job</label>
                            <select name="job_id">
                                <option value="0">No job</option>
                                @foreach($jobs as $job)
                                    <option value="{{ $job->id }}">{{ $job->position->title }}, {{ $job->company->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-field">
                            <label>Date</label>
                            <input name="memory_date" type='text' class="datepicker" />
                        </div>
                        <input type="hidden" name="type_id" value="">
                        <input type="hidden" name="file_name" value="">
                        <input type="hidden" name="source" value="">
                        <input type="hidden" name="fb_id" value="">
                        <input type="hidden" name="instagram_id" value="">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="full-width">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="import-facebook-memory" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h2>Facebook memories</h2>
                </div>
                <div class="modal-body">
                    <p>Importing your videos and photos from facebook. All persons tagged will get created in DIGI-X. Import?</p>
                </div>
                <div class="modal-footer">
                    <a href="{{ $facebook_link }}" class="social-link-import button">Import</a>
                    <button type="button" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="import-instagram-memory" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h2>Instagram memories</h2>
                </div>
                <div class="modal-body">
                    <p>Importing your videos and photos from instagram. All persons tagged will get created in DIGI-X. Import?</p>
                </div>
                <div class="modal-footer">
                    <a href="{{ $instagram_link }}" class="social-link-import button">Import</a>
                    <button type="button" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@endsection