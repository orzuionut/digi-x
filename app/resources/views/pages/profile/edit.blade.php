@extends ('layouts.master')

@section('title', 'Profile - Digi-X')
@section('body-class', 'profile edit-profile-template')

@section('content')
    <div class="page-title with-image">
        <div class="overlay"></div>
        <h1>Your profile</h1>
    </div>
    <div class="edit-profile section form-section">
        <div class="container">
            <form method="POST" action="{{ action('ProfileController@update') }}">
                <input type="hidden" name="_method" value="PUT">
                {!! csrf_field() !!}
                <div class="form-field">
                    <label>Name</label>
                    <input type="text" name="name" value="{{ $user->name }}">
                    {!! d_error('name', $errors->toArray()) !!}
                </div>
                <div class="form-field">
                    <label>Email</label>
                    <input type="text" readonly name="email" value="{{ $user->email }}">
                </div>
                <div class="form-field">
                    <label>Api token</label>
                    <input type="text" readonly name="email" value="{{ $user->api_token }}">
                </div>
                <button type="submit" class="full-width">Update profile</button>
            </form>
        </div>
    </div>
@endsection