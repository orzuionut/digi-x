@extends ('layouts.master')

@section('title', 'Edit trunk - Digi-X')
@section('body-class', 'trunks edit-trunk-template')

@section('content')
    <div class="page-title with-image">
        <div class="overlay"></div>
        <h1>Edit trunk</h1>
    </div>
    <div class="add-person section form-section">
        <div class="container">
            <form method="POST" action="{{ action('TrunkController@update', $trunk->id) }}">
                <input type="hidden" name="_method" value="PUT">
                {!! csrf_field() !!}
                <div class="form-field">
                    <label>Title</label>
                    <input type="text" name="title" value="{{ $trunk->title }}">
                    {!! d_error('title', $errors->toArray()) !!}
                </div>
                <div class="form-field">
                    <label>Description</label>
                    <textarea name="description">{{ $trunk->description }}</textarea>
                    {!! d_error('description', $errors->toArray()) !!}
                </div>
                <button type="submit" class="full-width">Save info</button>
            </form>
        </div>
    </div>
@endsection