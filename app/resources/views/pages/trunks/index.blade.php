@extends ('layouts.master')

@section('title', 'Listing trunks - Digi-X')
@section('body-class', 'trunks edit-trunk-template')

@section('content')
    <div class="page-title with-image">
        <div class="overlay"></div>
        <h1>Your trunks</h1>
        <input class="table-filter" type="text" name="trunks_filter" placeholder="Search">
        <a href="{{ route('trunks.create') }}" class="button add">
            <i class="fa fa-plus"></i>
        </a>
    </div>
    <div class="trunks-listing section listing-section">
        <div class="container small">
            @if ($trunks)
                @if( !($trunks->isEmpty() ) )
                    <div class="table table-filter resp-table">
                        <div class="head row">
                            <div class="cell">Title</div>
                            <div class="cell">Description</div>
                            <div class="cell">&nbsp;</div>
                        </div>
                        @foreach($trunks as $trunk)
                            <div class="row">
                                <div class="cell filter-cell">{{ str_limit($trunk->title, $limit = 20, $end = '...') }}</div>
                                <div class="cell filter-cell">{{ str_limit($trunk->description, $limit = 40, $end = '...') }}</div>
                                <div class="cell actions">
                                    <a href="{{ route('trunks.edit', $trunk->id) }}"><i
                                                class="fa fa-pencil"></i>Edit</a>
                                    <!--a href="#"><i class="fa fa-trash"></i>Delete</a-->
                                </div>
                            </div>
                        @endforeach
                    </div>
                @else
                    <p class="no-resource">You haven't added any trunks yet, get started <a href="{{ route('trunks.create') }}">here</a>.</p>
                @endif
            @endif
        </div>
    </div>
@endsection