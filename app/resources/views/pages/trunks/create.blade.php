@extends ('layouts.master')

@section('title', 'Add trunk - Digi-X')
@section('body-class', 'trunks create-trunk-template')

@section('content')
    <div class="page-title with-image">
        <div class="overlay"></div>
        <h1>Add trunk</h1>
    </div>
    <div class="create-person section form-section">
        <div class="container">
            <form method="POST" action="{{ action('TrunkController@store') }}">
                {!! csrf_field() !!}
                <div class="form-field">
                    <label>Title</label>
                    <input type="text" name="title" value="{{ old('title') }}">
                    {!! d_error('title', $errors->toArray()) !!}
                </div>
                <div class="form-field">
                    <label>Description</label>
                    <textarea name="description">{{ old('description') }}</textarea>
                    {!! d_error('description', $errors->toArray()) !!}
                </div>
                <button type="submit" class=" full-width">Add trunk</button>
            </form>
        </div>
    </div>
@endsection