@extends('layouts.blank')
@section('title', 'Login')

@section('content')
    <div class="auth-box box">
        <form method="POST" action="/login">
            {!! csrf_field() !!}
            @if (count($errors) > 0)
                <ul class="errors">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            <div class="form-field">
                <label for="email-input">Email</label>
                <input type="email" name="email" id="email-input" value="{{ old('email') }}">
            </div>
            <div class="form-field">
                <label for="password-input">Password</label>
                <input type="password" name="password" id="password-input">
            </div>
            <div>
                <div class="checkbox-cont">
                    <input type="checkbox" name="remember" id="remember-input"><label for="remember-input">Remember Me</label>
                </div>
            </div>
            <div>
                <button type="submit" class="full-width">Login</button>
            </div>
        </form>
    </div>
@endsection