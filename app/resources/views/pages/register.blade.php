@extends('layouts.blank')
@section('title', 'Register')

@section('content')
    <div class="auth-box box">
        <form method="POST" action="/register">
            {!! csrf_field() !!}
            @if (count($errors) > 0)
                <ul class="errors">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            <div class="form-field">
                <label>Name</label>
                <input type="text" name="name" value="{{ old('name') }}">
            </div>
            <div class="form-field">
                <label>Email</label>
                <input type="email" name="email" value="{{ old('email') }}">
            </div>
            <div class="form-field">
                <label>Password</label>
                <input type="password" name="password">
            </div>
            <div class="form-field">
                <label>Confirm Password</label>
                <input type="password" name="password_confirmation">
            </div>
            <div>
                <button type="submit" class="full-width">Register</button>
            </div>
            <!--div class="horizontal-line"><span class="horizontal-text">or</span></div>
            <div class="social-cont">
                <a href="#" class="social-icon facebook">
                    <i class="fa fa-facebook"></i>
                </a>
                <a href="#" class="social-icon google-plus">
                    <i class="fa fa-google-plus"></i>
                </a>
            </div-->
        </form>
    </div>
@endsection