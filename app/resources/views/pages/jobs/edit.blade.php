@extends ('layouts.master')

@section('title', 'Edit job - Digi-X')
@section('body-class', 'jobs-template create-job-template')

@section('content')
    <div class="page-title with-image">
        <div class="overlay"></div>
        <h1>Edit job</h1>
    </div>
    <div class="add-person section form-section">
        <div class="container">
            <form method="POST" action="{{ action('JobController@update', $job->id) }}">
                <input type="hidden" name="_method" value="PUT">
               @include('pages.jobs._form', ['submitButtonText' => 'Edit job'])
            </form>
        </div>
    </div>
@endsection