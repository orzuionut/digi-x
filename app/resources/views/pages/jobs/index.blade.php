@extends ('layouts.master')

@section('title', 'Listing jobs - Digi-X')
@section('body-class', 'jobs edit-job-template')

@section('content')
    <div class="page-title with-image">
        <div class="overlay"></div>
        <h1>Your jobs</h1>
        <input class="table-filter" type="text" name="jobs_filter" placeholder="Search">
        <a href="{{ route('jobs.create') }}" class="button add">
            <i class="fa fa-plus"></i>
        </a>
    </div>
    <div class="jobs-listing section listing-section">
        <div class="container small">
            @if ($jobs)
                @if( !($jobs->isEmpty() ) )
                    <div class="table table-filter resp-table">
                        <div class="head row">
                            <div class="cell">Company</div>
                            <div class="cell">Position</div>
                            <div class="cell">&nbsp;</div>
                        </div>
                        @foreach($jobs as $job)
                            <div class="row">
                                <div class="cell filter-cell">{{ str_limit($job->company->name, $limit = 20, $end = '...') }}</div>
                                <div class="cell filter-cell">{{ str_limit($job->position->title, $limit = 40, $end = '...') }}</div>
                                <div class="cell actions">
                                    <a href="{{ route('jobs.edit', $job->id) }}"><i class="fa fa-pencil"></i>Edit</a>
                                    <!--a href="#"><i class="fa fa-trash"></i>Delete</a-->
                                </div>
                            </div>
                        @endforeach
                    </div>
                @else
                    <p class="no-resource">You haven't added any jobs yet, get started <a href="{{ route('jobs.create') }}">here</a>.</p>
                @endif
            @endif
        </div>
    </div>
@endsection