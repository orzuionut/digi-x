{!! csrf_field() !!}
<div class="form-field">
    <label>New company</label>
    <input type="text" name="company_name">
</div>
<div class="form-field">
    <label>Existing company</label>
    <select name="existing_company_id">
        @foreach ($companies as $company)
            <option value="{{ $company->id }}">{{ $company->name }}</option>
        @endforeach
    </select>
</div>
<div class="form-field">
    <label>New position</label>
    <input type="text" name="position_title">
</div>
<div class="form-field">
    <label>Existing position</label>
    <select name="existing_position_id">
        @foreach ($positions as $position)
            <option value="{{ $position->id }}">{{ $position->title }}</option>
        @endforeach
    </select>
</div>
<button type="submit" class="full-width">{{ $submitButtonText }}</button>