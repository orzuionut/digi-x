@extends('layouts.master')

@section('title', 'Homepage')

@section('content')
    <div class="banner">
        <img src="{{ URL::asset('img/banner-homepage.png') }}" alt="#">
    </div>
    <div class="app-description">
        <div class="container-sm">
            <p class="intro">Digi-X provides you with a smart set of tools to allow you to save your awesome memories. Beautiful, simple and easy to use, Digi-x is a fun and friendly place to share your memories.</p>
            <div class="section add-your-memory cf">
                <div class="col">
                    <img src="{{ URL::asset('img/add-your-memories-icon.png') }}" alt="#">
                </div>
                <div class="col">
                    <h3>1. Add your memories</h3>
                    <p>Upload photos, videos, documents to keep them safe over the years and share them with the loved ones.</p>
                </div>
            </div>
            <div class="section add-your-memory cf">
                <div class="col">
                    <h3>2. Organize your memories</h3>
                    <p>Once uploaded, you can sort them using trunks. You can add tags making finding a long time memory a breeze. </p>
                </div>
                <div class="col">
                    <img src="{{ URL::asset('img/tag-and-arrange-memories-icon.png') }}" alt="#">
                </div>
            </div>
            <div class="section add-your-memory cf">
                <div class="col">
                    <img src="{{ URL::asset('img/save-money-icon.png') }}" alt="#">
                </div>
                <div class="col">
                    <h3>3. Save money</h3>
                    <p>Digi-X provides you with all necessary tools to manage your memories making paper albums obsolete.</p>
                </div>
            </div>
            <div class="call-to-action">
                <p>Ready to take advantage of all the outlined features?</p>
                <a class="button full-width" href="{{ route('memories.create') }}">Add your memory now</a>
            </div>
        </div>
    </div>
@endsection