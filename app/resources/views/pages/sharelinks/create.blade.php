@extends ('layouts.master')

@section('title', 'Add sharelink - Digi-X')
@section('body-class', 'persons create-person-template')

@section('content')
    <div class="page-title with-image">
        <div class="overlay"></div>
        <h1>Add sharelink</h1>
    </div>
    <div class="create-person section form-section">
        <div class="container">
            <form method="POST" action="{{ action('SharelinkController@store') }}">
                {!! csrf_field() !!}
                @if( !($memories->isEmpty() ) )
                    <div class="form-field">
                        <label>Memory name</label>
                        <select name="memory_id">
                            @foreach ($memories as $memory)
                                <option value="{{ $memory->id }}"
                                        @if($memory->id == old('memory_id')) selected
                                        @endif
                                >{{ $memory->title }}</option>
                            @endforeach
                        </select>
                        {!! d_error('memory_id', $errors->toArray()) !!}
                    </div>

                <button type="submit" class="full-width">Add sharelink</button>
                @else
                    <p class="no-resource">You have no memories to create sharelinks, get started <a
                                href="{{ route('memories.create') }}">here</a>.</p>
                @endif
            </form>
        </div>
    </div>
@endsection