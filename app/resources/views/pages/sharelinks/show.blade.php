@extends ('layouts.master')

@section('title', 'Memory - Digi-X')
@section('body-class', 'memories-template  show-memory-template')

@section('content')
    <div class="memory-file-cont type-{{ $memory->type->name }}">
        <div class="overlay"></div>
        <div class="vertical-align-helper"></div>
        @if($memory->type->name == 'image')
            <img class="memory-file"src="@if($memory->file_name){{url('sharelinks/read/' . $hash ) }}@else {{ $memory->source }} @endif" alt="#">
        @endif
    </div>
    <div class="show-memory section form-section">
        <div class="container">
            <div class="title">
                <h2>{{ $memory->title }}</h2>
            </div>
            <span class="date">
                <i class="fa fa-calendar-o"></i>
                {{ $memory->memory_date->diffForHumans() }}
            </span>
            <div class="content">
                <p>
                    {{ $memory->description }} -
                    @if ($memory->location)
                        <span><b>at</b> {{ $memory->location->name }}</span>
                    @endif
                    @if (!$memory->persons->isEmpty())
                        <span><b>with</b>
                            @foreach($memory->persons as $key => $person)
                                @if($key != 0),@endif
                                <a href="{{ route('persons.edit', $person->id) }}">{{ $person->display_name() }}</a>
                            @endforeach
                        </span>
                    @endif
                    @if ($memory->trunk)
                        <span><b>in</b> <a href="{{ route('trunks.edit', $memory->trunk->id) }}">{{ $memory->trunk->title }}</a></span>
                    @endif
                </p>
            </div>
        </div>
    </div>
@endsection