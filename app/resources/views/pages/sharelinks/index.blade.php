@extends ('layouts.master')

@section('title', 'Listing sharelinks - Digi-X')
@section('body-class', 'trunks edit-trunk-template')

@section('content')
    <div class="page-title with-image">
        <div class="overlay"></div>
        <h1>Your sharelinks</h1>
        <a href="{{ route('sharelinks.create') }}" class="button add">
            <i class="fa fa-plus"></i>
        </a>
    </div>
    <div class="trunks-listing section listing-section">
        <div class="container small">
            @if ($sharelinks)
                @if( !($sharelinks->isEmpty() ) )
                    <div class="table">
                        <div class="head row resp-table">
                            <div class="cell">Memory name</div>
                            <div class="cell">Sharelink</div>
                            <div class="cell">&nbsp;</div>
                        </div>
                        @foreach($sharelinks as $sharelink)
                            <div class="row">
                                <div class="cell">{{ str_limit($sharelink->memory->title, $limit = 20, $end = '...') }}</div>
                                <div class="cell">{{ url('/sharelinks/s/' . str_limit($sharelink->hashed_link, $limit = 10, $end = '...')) }}</div>
                                <div class="cell actions">
                                    <a href="javascript: return false;" class="clipbrd" title="Copied!" data-clipboard-text="{{ url('/sharelinks/s/' . $sharelink->hashed_link) }}"><i class="fa fa-copy"></i>Copy</a>
                                    <!--a href=""><i class="fa fa-trash"></i>Delete</a-->
                                </div>
                            </div>
                        @endforeach
                    </div>
                @else
                    <p class="no-resource">You haven't added any share links yet, get started <a href="{{ route('sharelinks.create') }}">here</a>.</p>
                @endif
            @endif
        </div>
    </div>
@endsection
