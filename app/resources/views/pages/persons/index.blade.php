@extends ('layouts.master')

@section('title', 'Listing jobs - Digi-X')
@section('body-class', 'jobs edit-job-template')

@section('content')
    <div class="page-title with-image">
        <div class="overlay"></div>
        <h1>Your persons</h1>
        <input class="table-filter" type="text" name="person_filter" placeholder="Search">
        <a href="{{ route('persons.create') }}" class="button add">
            <i class="fa fa-plus"></i>
        </a>
    </div>
    <div class="jobs-listing section listing-section">
        <div class="container small">
            @if ($persons)
                @if( !($persons->isEmpty() ) )
                    <div class="table table-filter resp-table">
                        <div class="head row">
                            <div class="cell">First name</div>
                            <div class="cell">Last name</div>
                            <div class="cell">Relationship</div>
                            <div class="cell">Date of birth</div>
                            <div class="cell">&nbsp;</div>
                        </div>

                        @foreach($persons as $person)
                            <div class="row">
                                <div class="cell filter-cell">{{ str_limit($person->first_name, $limit = 20, $end = '...') }}</div>
                                <div class="cell filter-cell">{{ str_limit($person->last_name, $limit = 20, $end = '...') }}</div>
                                <div class="cell filter-cell">{{ str_limit($person->relationshiptypes->name, $limit = 15, $end = '...') }}</div>
                                <div class="cell">{{ str_limit($person->date_of_birth, $limit = 10, $end = '') }}</div>
                                <div class="cell actions">
                                    <a href="{{ route('persons.edit', $person->id) }}"><i class="fa fa-pencil"></i>Edit</a>
                                    <!--a href="{{ route('persons.destroy', $person->id) }}"><i class="fa fa-trash"></i>Delete</a-->
                                </div>
                            </div>
                        @endforeach
                    </div>
                @else
                    <p class="no-resource">You haven't added any persons yet, get started <a href="{{ route('persons.create') }}">here</a>.</p>                @endif
            @endif
        </div>
    </div>
@endsection