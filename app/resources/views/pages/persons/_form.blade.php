{!! csrf_field() !!}
<div class="form-field">
    <label>First name</label>
    <input type="text" name="first_name" value="{{ old('first_name') }}">
    {!! d_error('first_name', $errors->toArray()) !!}
</div>
<div class="form-field">
    <label>Last name</label>
    <input type="text" name="last_name" value="{{ old('last_name') }}">
    {!! d_error('last_name', $errors->toArray()) !!}
</div>
<div class="form-field">
    <label>Date of birth</label>
    <input type="text" class="datepicker" name="date_of_birth" value="{{ old('date_of_birth') }}">
    {!! d_error('date_of_birth', $errors->toArray()) !!}
</div>
<div class="form-field">
    <label>Relationship</label>
    <select name="relationship_id">
        @foreach ($relationships as $relationship)
            <option value="{{ $relationship->id }}"
                    @if($relationship->id == old('relationship_id')) selected
                    @endif
            >{{ $relationship->name }}</option>
        @endforeach
    </select>
    {!! d_error('relationship_id', $errors->toArray()) !!}
</div>
<button type="submit" class="full-width">{{ $submitButtonText }}</button>