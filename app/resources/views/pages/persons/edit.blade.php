@extends ('layouts.master')

@section('title', 'Edit person - Digi-X')
@section('body-class', 'persons edit-person-template')

@section('content')
    <div class="page-title with-image">
        <div class="overlay"></div>
        <h1>Edit person</h1>
    </div>
    <div class="add-person section form-section">
        <div class="container">
            <form method="POST" action="{{ action('PersonController@update', $person->id) }}">
                <input type="hidden" name="_method" value="PUT">
                {!! csrf_field() !!}
                <div class="form-field">
                    <label>First name</label>
                    <input type="text" name="first_name" value="{{ $person->first_name }}">
                    {!! d_error('first_name', $errors->toArray()) !!}
                </div>
                <div class="form-field">
                    <label>Last name</label>
                    <input type="text" name="last_name" value="{{ $person->last_name }}">
                    {!! d_error('last_name', $errors->toArray()) !!}
                </div>
                <div class="form-field">
                    <label>Date of birth</label>
                    <input type="text" class="datepicker" name="date_of_birth" value="@if($person->date_of_birth){{ $person->date_of_birth->format('Y/m/d')  }}@endif">
                    {!! d_error('date_of_birth', $errors->toArray()) !!}
                </div>
                <div class="form-field">
                    <label>Relationship</label>
                    <select name="relationship_id">
                        @foreach ($relationships as $relationship)
                            <option value="{{ $relationship->id }}"
                                    @if($relationship->id == $person->relationship_id) selected
                                    @endif>{{ $relationship->name }}</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="full-width">Save info</button>
            </form>
        </div>
    </div>
@endsection