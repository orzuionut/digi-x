<!DOCTYPE html>
<html>
<head>
    @include('includes.head')
</head>
<body class="blank-layout">
<div id="content">
    <div class="logo">
        <h1><a href="{{ URL::to('/')  }}">Digi-X</a></h1>
    </div>
    @yield('content')
</div>
@include('includes.scripts')
</body>
</html>
