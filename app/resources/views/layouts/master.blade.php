<!DOCTYPE html>
<html>
<head>
    @include('includes.head')
</head>
<body class="@yield('body-class')">
@include('includes.header')
<div id="content">
    @yield('content')
</div>
@include('includes.footer')
@include('includes.scripts')
</body>
</html>
