<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Memories path
	|--------------------------------------------------------------------------
	|
	| Path to the where the memories are stored. The format will
	| be base/user_id/memory
	|
	*/

	'memories' => [
		'base' => storage_path() . '/memories/'
	],

];
