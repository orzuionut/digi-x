<?php
return [
	'validation' => [
		'image_mime_types' => [
			'png',
			'jpeg',
			'jpg'
		],
		'video_mime_types' => [
			'mp4',
			'qt',
			'mov',
			'avi',
			'webm',
			'ogv'
		],

		'doc_mime_types' => [
			'txt', //.txt
			'pdf', //.pdf
			'doc', //.doc
			'docx', //.docx
			'ppt', //.ppt
			'pptx', //.pptx
			'xls', //.xls
			'xml', //.xml
			'rtf' //.rtf
		],

		'audio_mime_types' => [
			'mp3', //.mp3
			'midi', //.midi
			'wav'
		],

		'max_size' => '10240' //kb => 10MB
	]
];