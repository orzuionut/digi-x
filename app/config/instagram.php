<?php

/*
 * This file is part of Laravel Instagram.
 *
 * (c) Vincent Klaiber <hello@vinkla.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Instagram app
    |--------------------------------------------------------------------------
    |
    | Here you specify the app credentials for instragram app.
    |
    */

    'app_id' => 'b2aabb7cb9d84f1cb9994c9212f89e5e',
    'app_secret' => 'eb26ff061696458f8e26c227230ea397'
];
