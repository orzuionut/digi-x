var clipboard = new Clipboard('.clipbrd');

clipboard.on('success', function(e) {
    console.log(e);
});

clipboard.on('error', function(e) {
    console.log(e);
});

$(document).ready(function() {
    $('.clipbrd').tooltipster({

        'interactive': true,
        'contentAsHTML': true,
        'autoClose': true,
        'trigger': 'click',
        'onlyOne': true
    })
});