(function ($, window, document, undefined) {
    'use strict';
    var DIGIX = window.DIGIX || {};

    DIGIX.location_field = {
        restrict_location: {},
        init: function () {
            this.bindUI();
        },
        bindUI: function () {
            this.initSearch($('input.location'));
        },
        initSearch: function ($input) {
            var _doSearch = function (query, syncResults) {
                var request = {
                        input: query
                    },
                    searchService = new google.maps.places.AutocompleteService();


                $input.closest('div').find('.icon-location').addClass('location-searching');

                searchService.getPlacePredictions(request, function (results) {
                    var arr = [];
                    if (results && results.length) {
                        $.each(results, function () {
                            var v = this, item;
                            // the Autocomplete service doesn't return geometry data,
                            // thus additional request to PlaceService is required for every
                            // interested place
                            item = {
                                value: v.description,
                                address: v.description,
                                name: v.description,
                                place_id: v.place_id,
                                source: 'google-maps-api',
                                type: 'location',
                                postcode: ''
                            };
                            arr.push(item);
                        });
                    }
                    syncResults(arr);
                    $input.closest('div').find('.icon-location').removeClass('location-searching');
                });
            };

            $input.typeahead({
                minLength: 2,
                hint: false
            },
            {
                name: 'google-search',
                source: _doSearch
            })
            .on("typeahead:selected", function(e,data,name){
                var $this = $(this);
                $('body').find('#tmp-gps').remove();
                $('body').append('<div id="tmp-gps" class="hidden"></div>');
                var placeService = new google.maps.places.PlacesService($('#tmp-gps')[0]);
                placeService.getDetails({placeId : data.place_id}, function(resp){

                    var parsedData = DIGIX.getAddressDataFromGeocoderResp(resp);
                    parsedData.latitude = resp.geometry.location.lat();
                    parsedData.longitude = resp.geometry.location.lng();
                    $('input[name="location_data"]').val(JSON.stringify(parsedData));
                });
            });
        }

    };


    window.DIGIX = DIGIX;
})(jQuery, window, document);