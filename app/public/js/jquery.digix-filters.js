(function ($, window, document, undefined) {
    'use strict';

    var DIGIX = window.DIGIX || {};

    DIGIX.filters = {
        $filterTrigger: null,
        $filterCont: null,
        $mainContent: null,
        $form: null,
        $mixItUpCont: null,
        $typeFilters: null,
        init: function(){
            var self = this;
            this.$filterTrigger = $('.filter-trigger');
            this.$filterCont = $('.filter-cont');
            this.$mainContent = $('.main-content');
            this.$mixItUpCont = $('.memories ul');
            this.$form = this.$filterCont.find('> form');
            this.$typeFilters = $('.memory-type-filters');

            this.bindUI();
            this.mobileFilter();

            this.mixItUpInit();
        },
        bindUI: function(){
            var self = this;

            //open/close lateral filter
            self.$filterTrigger.on('click', function(){
                self.triggerFilter(true);
            });

            // close filters
            self.$filterCont.find('.close').on('click', function(){
                self.triggerFilter(false);
            });

            //close filter dropdown inside lateral .filter
            self.$filterCont.find('.filter-block h4').on('click', function(){
                $(this).toggleClass('closed').siblings('.filter-content').slideToggle(300);
            });

            //fix lateral filter and gallery on scrolling
            $(window).on('scroll', function(){
                (!window.requestAnimationFrame) ? self.fixGallery() : window.requestAnimationFrame(self.fixGallery);
            });

            // filter by type
            $('.tab-filter li:not(.placeholder) a').on('click', function(e){
                e.preventDefault();
                self.typeFilter($(this));
            });

            // filter memories
            this.$form.on('submit', function(e){
                e.preventDefault();
                self.filter();
            });

        },
        mixItUpInit: function(){
            this.$mixItUpCont.mixItUp({
                controls: {
                    enable: false
                },
                callbacks: {
                    onMixStart: function(){
                        $('.fail-message').fadeOut(200);
                    },
                    onMixFail: function(){
                        $('.fail-message').fadeIn(200);
                    }
                }
            });
        },
        triggerFilter: function($bool) {
            var elementsToTrigger = $([
                $('.filter-trigger'),
                $('.filter-cont'),
                $('.tab-filter'),
                $('.memories')
            ]);
            elementsToTrigger.each(function(){
                $(this).toggleClass('filter-is-visible', $bool);
            });
        },
        fixGallery: function(){
            var offsetTop = $('.main-content').offset().top,
                scrollTop = $(window).scrollTop();
            ( scrollTop >= offsetTop ) ? $('.main-content').addClass('is-fixed') : $('.main-content').removeClass('is-fixed');
        },
        filter: function(){
            var self = this,
                data = self.$form.serialize();

            $.ajax({
                type: "POST",
                url: serverDigix.host + '/memories/filter',
                data: data,
                dataType: 'json',
                beforeSend: function(){
                    self.$form.find('input[type="submit"]').val("Loading..").attr('disabled', true).addClass('disabled');
                },
                success: function (data) {
                    self.$form.data('ids', data);
                    self.$mixItUpCont.mixItUp('filter', self.getFilterSelector());
                    self.$form.find('input[type="submit"]').val("Update").attr('disabled', false).removeClass('disabled');
                },
                error: function (data) {
                    self.$form.find('input[type="submit"]').val("Update").attr('disabled', false).removeClass('disabled');
                    var errors = data.responseJSON;
                    DIGIX.populateErrors(errors);
                }
            });
        },
        typeFilter: function ($btn) {
            var $filters = $btn.closest('.filters');

            $filters.find('a.selected').removeClass('selected');
            $btn.addClass('selected');

            this.$mixItUpCont.mixItUp('filter', this.getFilterSelector());
        },
        getFilterSelector: function(){ // this combines the sidebar filter with the top filter
            var ids = this.$form.data('ids'),
                typeFilter = this.$typeFilters.find('li:not(placeholder) a.selected').closest('li').data('filter');

            var selector = '';
            $.each(ids, function( index, value ){
                if(index != 0){
                    selector += ', '
                }
                selector += '.col[data-id="'+ value +'"]';

                if(typeFilter != 'all'){
                    selector += typeFilter;
                }

            });
            return selector;
        },

        mobileFilter: function(){
            //mobile version - detect click event on filters tab
            var filter_tab_placeholder = $('.tab-filter .placeholder a'),
                filter_tab_placeholder_default_value = 'Select',
                filter_tab_placeholder_text = filter_tab_placeholder.text();

            $('.tab-filter li a').on('click', function(e){
                e.preventDefault();

                //detect which tab filter item was selected
                var selected_filter = $(event.target).data('type');

                //check if user has clicked the placeholder item
                if( $(event.target).is(filter_tab_placeholder) ) {
                    (filter_tab_placeholder_default_value == filter_tab_placeholder.text()) ? filter_tab_placeholder.text(filter_tab_placeholder_text) : filter_tab_placeholder.text(filter_tab_placeholder_default_value) ;
                    $('.tab-filter').toggleClass('is-open');

                    //check if user has clicked a filter already selected
                } else if( filter_tab_placeholder.data('type') == selected_filter ) {
                    filter_tab_placeholder.text($(event.target).text());
                    $('.tab-filter').removeClass('is-open');

                } else {
                    //close the dropdown and change placeholder text/data-type value
                    $('.tab-filter').removeClass('is-open');
                    filter_tab_placeholder.text($(event.target).text()).data('type', selected_filter);
                    filter_tab_placeholder_text = $(event.target).text();
                }
            });
        }
    };

    window.DIGIX = DIGIX;
})(jQuery, window, document);