(function ($, window, document, undefined) {
    'use strict';

    var DIGIX = window.DIGIX || {};

    DIGIX.memories = {
        $deleteBtn: null,
        init: function () {
            var self = this;

            this.$deleteBtn = $('a.delete-memory');

            this.bindUI();
        },
        bindUI: function () {
            var self = this;

            this.$deleteBtn.on('click', function(e){
                e.preventDefault();
                self.deleteMemory($(this));
            });

        },
        deleteMemory: function ($btn) {
            var self = this;

            console.log($btn.attr('href'));

            $.ajax({
                type: "POST",
                url: $btn.attr('href'),
                data: {
                    _method: 'DELETE',
                    _token: $btn.data('token')
                },
                dataType: 'json',
                success: function (data) {
                    window.location.href = data.redirect_url;
                },
                error: function (data) {

                }
            });
        }
    };

    window.DIGIX = DIGIX;
})(jQuery, window, document);