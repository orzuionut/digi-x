(function ($, window, document, undefined) {
    'use strict';

    var DIGIX = window.DIGIX || {};

    DIGIX.jobs = {
        $newCompany: null,
        $existingCompany: null,
        $newPosition: null,
        $existingPosition: null,
        $form: null,
        init: function () {
            var self = this;

            this.$form = $('.add-job form');
            this.$newCompany = this.$form.find('[name="company_name"]');
            this.$existingCompany = this.$form.find('[name="existing_company_id"]');
            this.$newPosition = this.$form.find('[name="position_title"]');
            this.$existingPosition = this.$form.find('[name="existing_position_id"]');

            this.bindUI();
        },
        bindUI: function () {
            var self = this;

            self.$newCompany.on('change, keyup', self.handleCompanyFieldsState.bind(self));
            self.$form.on('submit', function(e){
                e.preventDefault();
                self.createJob.call(self, $(this));
            });

        },
        createJob: function ($form) {
            var self = this,
                data = $form.serialize();

            $.ajax({
                type: "POST",
                url: serverDigix.host + '/jobs',
                data: data,
                dataType: 'json',
                success: function (data) {
                    window.location.href = data.redirect_url;
                },
                error: function (data) {
                    var errors = data.responseJSON;
                    DIGIX.populateErrors(errors);
                }
            });
        },
        handleCompanyFieldsState: function(){
            var self = this;
            if(self.$newCompany.val() != ''){
                self.$existingCompany.dropdown("disable");

            } else {
                self.$existingCompany.dropdown("enable");
            }
        }
    };

    window.DIGIX = DIGIX;
})(jQuery, window, document);