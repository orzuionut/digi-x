(function ($, window, document, undefined) {
    'use strict';

    var DIGIX = window.DIGIX || {};

    DIGIX.persons = {
        $form: null,
        init: function () {
            var self = this;

            this.$form = $('.create-person form');

            this.bindUI();
        },
        bindUI: function () {
        }
    };

    window.DIGIX = DIGIX;
})(jQuery, window, document);