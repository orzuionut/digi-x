
(function ($, window, document, undefined) {
    'use strict';
    var DIGIX = window.DIGIX || {};

    DIGIX.flashNotification = {
        $cont: null,
        init: function () {
            this.$cont = $('.flash-notification');
            this.bindUI();

            if(this.$cont.find('.messages > *').length){
                this.notificationsOnRefresh();
            }
        },
        bindUI: function(){
            var self = this;
            $('.flash-notification .close').on('click', self.close.bind(self));
        },
        close: function(){
            this.$cont.slideUp('fast');
        },
        show: function(messages, type){
            this.$cont.removeClass('error success');
            this.$cont.addClass(type);

            var $messagesCont = this.$cont.find('.messages');
            $messagesCont.html('');
            $.each( messages , function( key, value ) {
                $messagesCont.append('<span>'+value+'</span>');
            });
            this.$cont.slideDown('fast');
        },
        notificationsOnRefresh: function(){
            this.$cont.slideDown('fast');
        }
    };

    DIGIX.getAddressDataFromGeocoderResp = function (item) {
        var arrAddress = item.address_components;
        var data = {};
        $.each(arrAddress, function (i, address_component) {
            if (address_component.types[0] == "locality") {
                data.city = address_component.long_name;
            }
            if (address_component.types[0] == "postal_code") {
                data.zip = address_component.long_name;
            }
            if (address_component.types[0] == "route") {
                data.street = address_component.long_name;
            }
            if (address_component.types[0] == "street_number") {
                //data.street_number = address_component.long_name;
            }
            if (address_component.types[0] == "sublocality_level_1") {
                data.state = address_component.long_name;
            }
            if (address_component.types[0] == "sublocality_level_2") {
                //data.sublocality_level_2 = address_component.long_name;
            }
            if (address_component.types[0] == "country") {
                data.country = address_component.long_name;
                data.country_code = address_component.short_name;
            }
        });

        return data;
    };

    DIGIX.populateErrors = function(errors){
        if(errors !== 'undefined' && 'general' in errors){
            DIGIX.flashNotification.show(errors.general);
            delete errors.general;
        }

        var parentsSelector = '.form-field, .filter-block';
        $.each( errors , function( key, value ) {
            var error = value[0],
                $fields = $('input[name="'+ key +'"], select[name="'+ key +'"], textarea[name="'+ key +'"]'),
                error_html = '<span class="error">'+ value[0] +'</span>';

            $fields.closest(parentsSelector).append(error_html);
            $fields.on('focus change', function(){
                $(this).closest(parentsSelector).find('span.error').fadeOut();
            });
        });
    };


    window.DIGIX = DIGIX;
})(jQuery, window, document);