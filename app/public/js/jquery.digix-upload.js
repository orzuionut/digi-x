(function ($, window, document, undefined) {
    'use strict';

    var DIGIX = window.DIGIX || {};

    DIGIX.uploader = {
        up: null,
        $filesList: null,
        $modal: null,
        //$uploadFiles: null,
        init: function () {
            this.$filesList = $('#files-list');
            this.$modal = $('#edit-memory');
            //this.$uploadFiles = $('#upload-files');
            this.setPlupload();
            this.bindUploader();
            this.up.init();

            this.bindUI();
        },
        bindUI: function () {
            var self = this,
                $body = $('body');

            $body.on('click', '#files-list a.edit', function (e) {
                e.preventDefault();
                self.editMemory.call(self, $(this));
            });
            $body.on('submit', '#edit-memory form', function (e) {
                e.preventDefault();
                self.createMemory.call(self, $(this));
            });
            $body.on('click', '#files-list a.clear, #files-list a.remove', function (e) {
                e.preventDefault();
                self.removeFile.call(self, $(this));
            });
            $body.on('click', '#files-list a.upload-by-url', function (e) {
                e.preventDefault();
                self.fileUploadURL($(this));
            });
            $body.on('click', 'a.social-link-import', function (e) {
                self.socialLinkState($(this));
            });

            //window.onbeforeunload = self.confirmUnLoad;

        },
        setPlupload: function () {
            this.up = new plupload.Uploader({
                runtimes: 'html5,flash,silverlight,html4',
                browse_button: 'plupload-browse-button',
                container: 'drag-drop-area',
                drop_element: 'drag-drop-area',
                file_data_name: 'file',
                headers: {
                    'X-Requested-With': 'XMLHttpRequest' // added such this can be recognized as ajax request
                },
                url: serverDigix.host + '/memories/upload',
                flash_swf_url: './vendor/plupload/plupload.flash.swf',
                silverlight_xap_url: './vendor/plupload/plupload.silverlight.xap',
                filters: {
                    max_file_size: '64mb'
                }
            });
        },
        bindUploader: function () {
            var self = this;
            //this.$uploadFiles.on('click', function(e) {
            //    e.preventDefault();
            //    self.up.start();
            //});
            this.up.bind('Init', this.dragInit);
            this.up.bind('FilesAdded', this.filesAdded.bind(this));
            this.up.bind('FileUploaded', this.fileUploaded.bind(this));
            this.up.bind('UploadProgress', this.uploadProgress.bind(this));
            this.up.bind('Error', this.error.bind(this));
        },
        dragInit: function () {
            var $cont = $("#drag-drop-area");
            $cont.on('dragover', function () {
                if (!$(this).hasClass('dragover')) {
                    $(this).addClass('dragover');
                }
            });
            $cont.on('dragenter', function () {
                if (!$(this).hasClass('dragover')) {
                    $(this).addClass('dragover');
                }
            });
            $cont.on('dragleave', function () {
                if ($(this).hasClass('dragover')) {
                    $(this).removeClass('dragover');
                }
            });
            $cont.on('drop', function () {
                if ($(this).hasClass('dragover')) {
                    $(this).removeClass('dragover');
                }
            });
        },
        filesAdded: function (up, files) {
            var html = '';
            plupload.each(files, function (file) {
                html += '<div class="file" id="' + file.id + '">';
                html += '<span class="type"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></span>';
                html += '<span class="name">' + file.name + '</span>';
                html += '<span class="size"><b>(' + plupload.formatSize(file.size) + ')</b></span>';
                html += '<span class="status">';
                html += '<span class="message"></span>';
                html += '<div class="progress-bar">';
                html += '<div class="helper"></div>';
                html += '</div>';
                html += '</span>';
                html += ' </div>';
            });
            this.$filesList.append(html);
            up.start();
        },
        fileUploaded: function (up, file, response) {
            var sr = $.parseJSON(response.response),
                $file = this.$filesList.find('#' + file.id),
                $status = $file.find('.status'),
                $type = $file.find('.type');

            if (sr.error) { // show error
                this.addErrorAndClear($file, sr.error);
            } else { // add data from server, and show edit button
                $file.data('file_name', sr.file_name);
                $file.data('type_id', sr.type_id);

                if (sr.file_name != file.name) {
                    //$file.find('.name').text($file.find('.name').text()+' new: ('+sr.file_name+')');
                }
                $type.text(sr.type);

                var $edit = '<a href="#" class="edit">Edit</a>';
                $status.find('.message').html($edit);
            }
        },
        fileUploadURL: function ($btn) {
            var $file = $btn.closest('.file'),
                data = $file.data('prepopulate');

            $.ajax({
                type: "POST",
                url: serverDigix.host + '/memories/upload',
                data: {
                    url: data.source
                },
                dataType: 'json',
                beforeSend: function () {
                    $file.find('.status .progress-bar > div').css('height', '12%');
                    $file.find('.status .progress-bar > div').css('height', '28%');
                },
                success: function (sr) {
                    var $status = $file.find('.status'),
                        $type = $file.find('.type');

                    $file.data('source', sr.source);
                    $file.data('type_id', sr.type_id);

                    $file.find('.status .progress-bar > div').css('height', '100%');

                    $type.text(sr.type);
                    var $edit = '<a href="#" class="edit">Edit</a>';
                    $status.find('.message').html($edit);
                },
                error: function (data) {
                    var sr = data.responseJSON,
                        $status = $file.find('.status'),
                        $type = $file.find('.type');

                    var $clear = '<a href="#" class="clear">Clear</a>';
                    $status.addClass('error').find('.message').html(sr.error + ' - ' + $clear);
                    $type.addClass('error').html('Error');
                }
            });
        },
        uploadProgress: function (up, file) {
            var $file = this.$filesList.find('#' + file.id);
            $file.find('.status .progress-bar > div').css('height', file.percent + '%');
        },
        error: function (up, err) {
            var $file = this.$filesList.find('#' + err.file.id),
                error;

            if(err.response !== 'undefined'){
                err.response = $.parseJSON(err.response);
                error = err.response.file[0];
            } else {
                error = err.message;
            }

            this.addErrorAndClear($file, error);
        },
        editMemory: function ($editButton) {
            var $file = $editButton.closest('.file');

            // empty previous info
            this.$modal.find('input, textarea').not('[name="_token"]').val('');
            this.$modal.find('span.error').remove();

            // populate with new
            this.prepopulateModal($file);
            this.$modal.modal('show');
        },
        createMemory: function ($form, $file) {
            var self = this,
                data = $form.serialize(),
                $modal = $('#edit-memory');

            $.ajax({
                type: "POST",
                url: serverDigix.host + '/memories',
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    self.setButtonState($form.find('[type="submit"]'), true);
                },
                success: function () {
                    self.setButtonState($form.find('[type="submit"]'), false);
                    $modal.modal('hide');
                    DIGIX.flashNotification.show(
                        ['Memory was created!'], 'success'
                    );

                    self.$filesList.find('#' + $modal.data('file_id')).slideUp('slow', function() {
                        $(this).remove();
                    });
                },
                error: function (data) {
                    self.setButtonState($form.find('[type="submit"]'), false);
                    var errors = data.responseJSON;
                    DIGIX.populateErrors(errors);
                }
            });

        },
        prepopulateModal: function ($file) {
            this.$modal.data('file_id', $file.attr('id'));
            this.$modal.find('[name="type_id"]').val($file.data('type_id'));

            // set either file name for local files or source for remote
            if ($file.data('file_name')) {
                this.$modal.find('[name="file_name"]').val($file.data('file_name'));
            } else {
                this.$modal.find('[name="source"]').val($file.data('source'));
            }

            // prepopulate with data on social network
            var data = $file.data('prepopulate');
            if (!data) {
                return;
            }

            // set the title
            if (data.title) {
                this.$modal.find('[name="title"]').val(data.title);
            }

            // social id
            if (data.fb_id) {
                this.$modal.find('[name="fb_id"]').val(data.fb_id);
            } else if(data.instagram_id){
                this.$modal.find('[name="instagram_id"]').val(data.instagram_id);
            }

            // set the place
            if (data.location) {
                this.$modal.find('[name="location_name"]').val(data.location.name);
                delete data.location.name;
                delete data.location.user_id;

                this.$modal.find('input[name="location_data"]').val(JSON.stringify(data.location));
            }

            // set the persons
            if (data.persons) {
                var $select = this.$modal.find('[name="persons[]"]'),
                    selectize = $select[0].selectize,
                    values = $.map(data.persons, function (o) {
                        return o["id"];
                    });

                selectize.setValue(values);
            }

            // set the date
            if (data.memory_date) {
                this.$modal.find('[name="memory_date"]').val(data.memory_date);
            }

            this.$modal.find('[name="file_name"]').val($file.data('file_name'));
        },
        addErrorAndClear: function ($file, error) {
            var $type = $file.find('.type'),
                $clear = '<a href="#" class="clear">Clear</a>',
                $status = $file.find('.status');

            $status.addClass('error').find('.message').html(error + ' - ' + $clear);
            $type.addClass('error').html('Error');
        },
        socialLinkState: function ($btn) {
            $btn.addClass('disabled').attr('disabled', true).text('Importing..');
        },
        removeFile: function ($btn) {
            $btn.closest('.file').slideUp('slow', function() {
                $(this).remove();
            });
        },
        setButtonState: function ($btn, disabled) {
            if (disabled) {
                $btn.addClass('disabled').attr('disabled', true).text('Loading..');
            } else {
                $btn.removeClass('disabled').attr('disabled', false).text('Save changes');
            }
        },
        confirmUnLoad: function(e){
            var e = e || window.event,
                message = 'You have files that were uploaded but not saved.'

            if($('#files-list .file').length){
                if (e) {
                    e.returnValue = message
                }

                // For Safari
                return message;
            }
        }

    };

    window.DIGIX = DIGIX;
})(jQuery, window, document);