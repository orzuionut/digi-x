(function ($, window, document, undefined) {
    'use strict';

    var DIGIX = window.DIGIX || {};


    DIGIX.formstone = function () {
        $("input[type=checkbox], input[type=radio]").checkbox();
        $("select").not('.selectize').dropdown();
    };

    DIGIX.selectize = function () {
        $("select.selectize").selectize({});
    };

    DIGIX.tableSearch = function () {
        var $rows = $('.table .row:not(.head)');
        $('.table-filter').keyup(function () {
            var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

            $rows.show().filter(function () {
                var text = $(this).find('.filter-cell').text().replace(/\s+/g, ' ').toLowerCase();
                return !~text.indexOf(val);
            }).hide();
        });
    };

    DIGIX.respTables = function () {
        var $tables = $('.table.resp-table');

        $tables.each(function(){
            var $head = $(this).find('.row.head'),
                $rows = $(this).find('.row:not(.head)');

            $rows.each(function(){
                var $cells = $(this).find('.cell');
                $cells.each(function(index){
                    $(this).attr('data-head_label', $head.find('.cell').eq(index).text());
                });
            });
        });

    };

    DIGIX.datepicker = function () {
        $('input.datepicker').datetimepicker({
            format: 'DD/MM/YYYY',
            icons: {
                time: 'fa fa-time',
                date: 'fa fa-calendar',
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down',
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        });
    };

    DIGIX.errorsFade = function () {
        $('.form-field input, .form-field select, .form-field textarea').on('focus change', function () {
            $(this).closest('.form-field').find('span.error').fadeOut();
        });
    };

    // ready
    $(document).ready(function (e) {
        var bodyClasses = $('body').attr('class').split(/\s+/);

        DIGIX.flashNotification.init();
        DIGIX.formstone();
        DIGIX.datepicker();
        DIGIX.selectize();
        DIGIX.errorsFade();
        DIGIX.tableSearch();
        DIGIX.respTables();

        // persons page
        if ($.inArray('memories-template', bodyClasses) > -1) {
            DIGIX.memories.init();
        }

        // memories list
        if ($.inArray('create-memory-template', bodyClasses) > -1) {
            DIGIX.uploader.init();
            DIGIX.location_field.init();
        }

        // memories list
        if ($.inArray('memories-list-template', bodyClasses) > -1) {
            DIGIX.filters.init();
        }
        // jobs page
        if ($.inArray('create-job-template', bodyClasses) > -1) {
            DIGIX.jobs.init();
        }
        // persons page
        if ($.inArray('persons', bodyClasses) > -1) {
            DIGIX.persons.init();
        }

    });

    // load
    $(window).load(function () {

    });

    // resize
    $(window).resize(function () {
    });

    // scroll
    $(window).scroll(function () {
    });


})(jQuery, window, document)