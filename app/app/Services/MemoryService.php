<?php namespace App\Services;

use App\Models\Memory;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;

class MemoryService extends Service {

	private $errors = array();

	/**
	 * Filters memories
	 * @todo - maybe replace with a more elegant solution for the query
	 * @param $rules
	 *
	 * @return array
	 */
	public function filter( $rules ) {
		$user     = Auth::user();
		$response = array();


		$query = "SELECT DISTINCT m.id FROM memories m
				  LEFT JOIN jobs j ON j.ID = m.job_id
				  LEFT JOIN companies c ON c.ID = j.company_id
				  LEFT JOIN positions pos ON pos.ID = j.position_id
				  LEFT JOIN memory_person mp ON mp.memory_id = m.ID
				  LEFT JOIN persons per ON per.id = mp.person_id
				  LEFT JOIN relationship_types rt ON rt.id = per.relationship_id
				  WHERE m.user_id = :user_id ";
		$bind  = [ 'user_id' => $user->id ];


		// string search
		if ( !empty($rules['title_and_content']) ) {
			$query .= " AND (m.title LIKE :title_input OR m.description LIKE :desc_input)";
			$bind['title_input'] = $bind['desc_input'] = "%".$rules['title_and_content']."%";
		}

		// single value filters
		if ( !empty($rules['trunk_id']) ) {
			$query .= " AND m.trunk_id = :trunk_id";
			$bind['trunk_id'] = $rules['trunk_id'];
		}
		if ( !empty($rules['location_id']) ) {
			$query .= " AND m.location_id = :location_id";
			$bind['location_id'] = $rules['location_id'];
		}


		// persons - AND
		if ( !empty($rules['persons']) ) {
			$query .= " AND (";
			foreach ( $rules['persons'] as $key => $person_id ) {
				if($key != 0){
					$query .= " AND ";
				}
				$param_key = 'person_id'.$key;
				$query .= "per.id = :{$param_key}";
				$bind[$param_key] = $person_id;
			}

			$query .= ")";
		}

		if ( !empty($rules['relationships_names']) ) {
			$query .= " AND (";
			foreach ( $rules['relationships_names'] as $key => $relationship_name ) {
				if($key != 0){
					$query .= " OR ";
				}
				$param_key = 'relationship_name'.$key;
				$query .= "rt.name = :{$param_key}";
				$bind[$param_key] = $relationship_name;
			}

			$query .= ")";
		}

		if ( !empty($rules['relationships_degrees']) ) {
			$query .= " AND (";
			foreach ( $rules['relationships_degrees'] as $key => $relationship_degree ) {
				if($key != 0){
					$query .= " OR ";
				}
				$param_key = 'relationship_degree'.$key;
				$query .= "rt.degree = :{$param_key}";
				$bind[$param_key] = $relationship_degree;
			}

			$query .= ")";
		}

		// companies - OR
		if ( !empty($rules['companies']) ) {
			$query .= " AND (";
			foreach ( $rules['companies'] as $key => $company_id ) {
				if($key != 0){
					$query .= " OR ";
				}
				$param_key = 'company_id'.$key;
				$query .= "c.id = :{$param_key}";
				$bind[$param_key] = $company_id;
			}

			$query .= ")";
		}

		// positions - OR
		if ( !empty($rules['positions']) ) {
			$query .= " AND (";
			foreach ( $rules['positions'] as $key => $position_id ) {
				if($key != 0){
					$query .= " OR ";
				}
				$param_key = 'position_id'.$key;
				$query .= "pos.id = :{$param_key}";
				$bind[$param_key] = $position_id;
			}
			$query .= ")";
		}

		// date query
		if(!empty($rules['date_range_or_single'])){
			$dates = $this->parse_date($rules['date_range_or_single'], 'date_range_or_single');
			if($dates){
				if(!empty($dates['to'])){
					$query .= " AND ( m.memory_date >= :from_date AND m.memory_date <= :to_date )";
					$bind['from_date'] = $dates['from']->format('Y-m-d');
					$bind['to_date'] = $dates['to']->format('Y-m-d');
				} else {
					$query .= " AND ( m.memory_date = :the_date )";
					$bind['the_date'] = $dates['from']->format('Y-m-d');
				}
			} else {
				$response['errors'] = $this->errors;
				return $response;
			}
		}



		// run query and return ids
		$memories = DB::select( $query, $bind );
		$memories_ids = [];
		foreach($memories as $key => $memory){
			$memories_ids[$key] = $memory->id;
		}

		$response['memories_ids'] = $memories_ids;

		return $response;
	}

	/**
	 * Parses date
	 *
	 * @param $string
	 * @param $key
	 *
	 * @return bool|Carbon[]
	 */
	private function parse_date($string, $key){
		$delim = ' to ';
		if (strpos($string, $delim) === false) { // if to is not present
			$from = $string;
			if(strtotime($from) === false){
				$this->errors[$key][] = 'Date format is not correct.';
				return false;
			}
			$dates['from'] = new Carbon($from);
		} else {
			list($from, $to) = explode(" to ", $string);
			if(strtotime($from) === false){
				$this->errors[$key][] = 'FROM date format is not correct.';
				return false;
			}
			if(strtotime($to) == false){
				$this->errors[$key][] = 'TO date format is not correct.';
				return false;
			}

			$dates['from'] = new Carbon($from);
			$dates['to'] = new Carbon($to);
		}

		return $dates;
	}
}
