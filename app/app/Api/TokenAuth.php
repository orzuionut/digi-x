<?php namespace App\Api;

use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Route;
use Dingo\Api\Contract\Auth\Provider;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class TokenAuth implements Provider {
	public function authenticate( Request $request, Route $route ) {
		if($request->user_id && $request->token){
			$user = User::where('api_token', $request->token)->first();
			if($user && $user->id == $request->user_id){
				return $user;
			} else {
				throw new UnauthorizedHttpException( 'Authentication issue.' );
			}
		}
		throw new UnauthorizedHttpException( 'Authentication issue.' );
	}
}