<?php namespace App\Api\V1\Controllers;


use App\Models\Trunk;
use App\User;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * @Resource("Users", uri="/users")
 */
class UserController extends ApiController {

	/**
	 * @param $id
	 *
	 * @return mixed
	 */
	public function show( $id ) {
		$user = User::find( $id );

		if(!$user){
			throw new NotFoundHttpException('User not found');
		}

		return $user;
	}


	/**
	 * Returns user trunks
	 *
	 * @param $id
	 *
	 * @return mixed
	 */
	public function trunks($id){
		$user = User::find( $id );

		if(!$user){
			throw new NotFoundHttpException('User not found');
		}

		$trunks = $user->trunks;
		return $trunks;
	}

	/**
	 * Creates a trunk for the user
	 *
	 * @return Trunk
	 */
	public function store_trunk( Request $request){
		$request_content = $request->getContent();
		$data = json_decode($request_content, true);

		$rules = [
			'title' => 'required|string',
			'description'  => 'string'
		];

		$validator = app('validator')->make($data, $rules);

		if ($validator->fails()) {
			throw new StoreResourceFailedException('Could not create new trunk.', $validator->errors());
		}

		// create the trunk
		$trunk = new Trunk();
		$trunk->title = $data['title'];
		if($data['description']){
			$trunk->description = $data['description'];
		}
		$trunk->user_id = $request->user_id;
		$trunk->save();

		return $trunk;
	}


	/**
	 * Updates a trunk
	 *
	 * @return mixed
	 */
	public function update_trunk(){
		$rules = [
			'title' => 'required|string',
			'description'  => 'string'
		];
		$request = app('request');
		$payload = $request->only('title', 'description');
		$validator = app('validator')->make($payload, $rules);

		if ($validator->fails()) {
			throw new StoreResourceFailedException('Could not update trunk.', $validator->errors());
		}

		$trunk = Trunk::find($request->user_id);
		$trunk->title = $request->title;
		if($request->description){
			$trunk->description = $request->description;
		}
		$trunk->save();


		return $trunk;
	}

	/**
	 * Returns user memories
	 *
	 * @param $id
	 *
	 * @return mixed
	 */
	public function memories($id){
		$user = User::find( $id );

		if(!$user){
			throw new NotFoundHttpException('User not found');
		}

		$memories = $user->memories()->with('persons')->get();


		return $memories;
	}

	/**
	 * @param $id
	 *
	 * @return mixed
	 */
	public function persons($id){
		$user = User::find( $id );

		if(!$user){
			throw new NotFoundHttpException('User not found');
		}

		$persons = $user->persons;
		return $persons;
	}

	/**
	 * @param $id
	 *
	 * @return mixed
	 */
	public function jobs($id){
		$user = User::find( $id );


		if(!$user){
			throw new NotFoundHttpException('User not found');
		}

		$jobs = $user->jobs()->with('company', 'position')->get();
		foreach($jobs as $job){
			$hidden = $job->getHidden();
			$job->setHidden(array_merge($hidden, ['company_id', 'position_id']));
		}

		return $jobs;
	}
}