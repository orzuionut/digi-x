<?php namespace App\Api\V1\Controllers;


use App\Models\Memory;
use App\Models\RelationshipType;
use App\User;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * @Resource("Relationships", uri="/relationships")
 */
class RelationshipController extends ApiController {

	/**
	 * @param $id
	 *
	 * @return mixed
	 */
	public function show( $id ) {
		$relationship_type = RelationshipType::find( $id );

		if(!$relationship_type){
			throw new NotFoundHttpException('Relationship not found');
		}
		return $relationship_type;
	}
}