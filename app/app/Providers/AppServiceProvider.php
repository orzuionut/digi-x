<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use JavaScript;
use Illuminate\Support\Facades\Request;
use Larabros\Elogram\Client;


class AppServiceProvider extends ServiceProvider {
	/**
	 * Bootstrap any application services.
	 * @return void
	 */
	public function boot() {
		// send host for use in javascript
		JavaScript::put( [
			'host' => url( '/' ),
		] );
	}

	/**
	 * Register any application services.
	 * @return void
	 */
	public function register() {
		// bind instagram client
		$this->app->bind( Client::class, function ( $app, $params ) {
			$app_id       = Config::get( 'instagram.app_id' );
			$app_secret   = Config::get( 'instagram.app_secret' );
			$redirect_url = url( 'memories/instagram-import' );


			if(!$params){
				$token = null;
			} else {
				$token = $params[0];
			}
			return new Client( $app_id, $app_secret, $token, $redirect_url );
		} );
	}
}
