<?php

namespace App\Events;

use App\Events\Event;
use App\Models\Sharelink;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ShareLinkAccessed extends Event {
	use SerializesModels;
	public $sharelink;

	/**
	 * ShareLinkAccessed constructor.
	 *
	 * @param Sharelink $sharelink
	 */
	public function __construct(Sharelink $sharelink) {
		$this->sharelink = $sharelink;
	}

	/**
	 * Get the channels the event should be broadcast on.
	 * @return array
	 */
	public function broadcastOn() {
		return [ ];
	}
}
