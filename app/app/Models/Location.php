<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model {

	/**
	 * Creates a new location if we don't already have a exact name
	 *
	 * @param $user_id
	 * @param $name
	 * @param $data
	 *
	 * @return static
	 */
	public static function handleCreate($user_id, $name, $data){
		if(!empty($data['id'])){
			$location = Location::find($data['id']);
		} else {
			$results = self::where('user_id', $user_id)->where('name', $name)->get();
			if($results->count()){
				return $results->first();
			}

			$data['name'] = $name;
			$data['user_id'] = $user_id;
			$location = self::create($data);
		}


		return $location;
	}

	protected $guarded = [];

	public $timestamps = false;
}
