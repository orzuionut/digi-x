<?php

namespace App\Models;

use App\Digix\File;
use App\Http\Requests\MemoryUploadRequest;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;


class Memory extends Model {

	use SoftDeletes;

	protected $dates = [ 'created_at', 'updated_at', 'deleted_at', 'memory_date' ];

	/**
	 * @param $date
	 */
	public function setMemoryDateAttribute( $date ) {
		$this->attributes['memory_date'] = Carbon::createFromFormat('d/m/Y', $date);
	}

	/**
	 * Persons tagged in the memory
	 */
	public function persons() {
		return $this->belongsToMany( 'App\Models\Person');
	}

	/**
	 * Trunk for the memory
	 *
	 * @return mixed
	 */
	public function trunk(){
		return $this->hasOne('App\Models\Trunk', 'id', 'trunk_id');
	}

	/**
	 * User for the memory
	 *
	 * @return mixed
	 */
	public function user(){
		return $this->hasOne('App\User', 'id', 'user_id');
	}

	/**
	 * Job for the memory
	 *
	 * @return mixed
	 */
	public function job(){
		return $this->hasOne('App\Models\Job', 'id', 'job_id');
	}

	/**
	 * Location for the memory
	 *
	 * @return mixed
	 */
	public function location(){
		return $this->hasOne('App\Models\Location', 'id', 'location_id');
	}

	/**
	 * Location for the memory
	 *
	 * @return mixed
	 */
	public function type(){
		return $this->hasOne('App\Models\MemoryType', 'id', 'type_id');
	}


	/**
	 * Upload memory
	 *
	 * if we upload the file locally, we set the file name
	 * and then set source if from social media
	 *
	 * @return mixed
	 */
	public function upload($source = '') {
		$user_id = Auth::id();
		$directory                = $this->get_user_directory();
		// create dir if it doesn't exist
		if ( ! file_exists( $directory ) ) {
			mkdir( $directory, 0777, true );
		}

		// get the file
		if($source){
			$file = new File($source, true);
		} else {
			$file = new File(Input::file('file'));;
		}

		// set new name
		$file_name = $this->get_unique_filename( $file->getFileName(), $user_id );

		// Try to upload file
		try {
			$response['success'] = false;
			$memory_type = $this->getType( $file->getFileExt() );

			if ( $memory_type ) { // if we don't have a memory type, we don't have a correct mimetype
				$response['type_id']   = $memory_type->id;
				$response['type']      = $memory_type->name;


				if($source){
					$response['source'] = $source;
				} else {
					$response['file_name'] = $file_name;
					$file->move($directory, $file_name);
				}

				$response['success'] = true;
			}

			return $response;
		} catch( \Exception $e ) {
			$errors              = array(
					$e->getMessage()
			);
			$response['success'] = false;
			$response['error']   = $errors[0];
		}

		return $response;
	}

	/**
	 * Return general type based on mimetype
	 * video | image | audio | document
	 *
	 * @param $mime_type
	 *
	 * @return null|string
	 */
	public function getType( $mime_type ) {
		$image_mime_types = Config::get('memory.validation.image_mime_types');
		$video_mime_types = Config::get('memory.validation.video_mime_types');
		$doc_mime_types = Config::get('memory.validation.doc_mime_types');
		$audio_mime_types = Config::get('memory.validation.audio_mime_types');

		if ( in_array( $mime_type, $video_mime_types ) ) {
			return MemoryType::where( 'name', 'video' )->first();
		} else if ( in_array( $mime_type, $image_mime_types ) ) {
			return MemoryType::where( 'name', 'image' )->first();
		} else if ( in_array( $mime_type, $audio_mime_types ) ) {
			return MemoryType::where( 'name', 'audio' )->first();
		} else if ( in_array( $mime_type, $doc_mime_types ) ) {
			return MemoryType::where( 'name', 'document' )->first();
		} else {
			return null;
		}
	}


	/**
	 * Returns unique filename
	 * e.g. avatar.jpg exists, it returns avatar (1).jpg
	 *
	 * @param $file_name
	 * @param $user_id
	 *
	 * @return mixed
	 */
	public function get_unique_filename( $file_name, $user_id ) {
		$directory = $this->get_user_directory($user_id);
		$file_path = $directory . $file_name;
		while( file_exists( $file_path ) ) {
			$file_name = $this->upcount_name( $file_name );
			$file_path = $directory . $file_name;
		}

		return $file_name;
	}

	/**
	 * Implements adding (1), (2) in order to not overwrite the images
	 *
	 * @param $name
	 *
	 * @return mixed
	 */
	protected function upcount_name( $name ) {
		return preg_replace_callback(
			'/(?:(?: \(([\d]+)\))?(\.[^.]+))?$/',
			array( $this, 'upcount_name_callback' ),
			$name,
			1
		);
	}

	protected function upcount_name_callback( $matches ) {
		$index = isset( $matches[1] ) ? ( (int) $matches[1] ) + 1 : 1;
		$ext   = isset( $matches[2] ) ? $matches[2] : '';

		return ' (' . $index . ')' . $ext;
	}

	/**
	 * Returns memories directory for a user/current user
	 *
	 * @param int $user_id
	 *
	 * @return string
	 */
	protected function get_user_directory($user_id = 0){
		if(!$user_id){
			$user_id = Auth::id();
		}
		$base_directory           = Config::get('path.memories.base');
		$directory                = $base_directory . $user_id;
		$directory                = rtrim( $directory, '/' ) . DIRECTORY_SEPARATOR;

		return $directory;
	}

	/**
	 * @param $file_name
	 * @param int $user_id
	 *
	 * @return string
	 */
	public function get_file_path($file_name, $user_id = 0){
		$directory                = $this->get_user_directory($user_id);
		$file = $directory . $file_name;

		return $file;
	}

	/**
	 * @return mixed
	 */
	public function ext(){
		if($this->source){
			$split = explode("/", $this->url);
			$file_name = array_pop($split);

			$split = explode("?", $file_name);
			$file_name = $split[0];
		} else {
			$file_name = $this->file_name;
		}
		$ext = pathinfo($file_name, PATHINFO_EXTENSION);

		return $ext;
	}

	/**
	 * Deletes a file from user memories
	 * used to remove the files that are not actually added (as the user refresh the page or something)
	 *
	 * @param $file_name
	 */
	public function delete_file($file_name, $user_id = 0){
		$file = $this->get_file_path($file_name, $user_id);
		if (file_exists($file)) {
			unlink($file);
		} else {
			Log::warning('Tried to delete a non-existing file.');
		}
	}
}
