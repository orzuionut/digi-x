<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RelationshipType extends Model
{
    public function Person(){
        return  $this->belongsToMany( 'App\Models\Person', 'id');
    }
}
