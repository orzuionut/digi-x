<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sharelink extends Model
{
    /**
     * Returns user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo('App\User');
    }

    /**
     * Return memory
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function memory(){
        return $this->belongsTo('App\Models\Memory');
    }
}
