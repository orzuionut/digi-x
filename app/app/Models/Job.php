<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model {
	
	public function company() {
		return $this->hasOne( 'App\Models\Company', 'id', 'company_id');
	}

	public function position() {
		return $this->hasOne( 'App\Models\Position', 'id', 'position_id');
	}

	/**
	 * The memories that have this job tagged
	 */
	public function memories() {
		return $this->belongsToMany( 'App\Models\Memory' );
	}

}
