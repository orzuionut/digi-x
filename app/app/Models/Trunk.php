<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trunk extends Model
{
	/**
	 * Returns user
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
    public function user(){
	    return $this->belongsTo('App\User');
    }
}
