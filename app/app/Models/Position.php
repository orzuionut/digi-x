<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
	public function is_owner($user_id){
		return $this->user_id == $user_id;
	}

	public function jobs(){
		return $this->belongsToMany('App\Models\Jobs');
	}
}
