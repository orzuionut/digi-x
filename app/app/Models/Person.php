<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Person extends Model {

	/**
	 * The table associated with the model.
	 * @var string
	 */
	protected $table = 'persons';

	protected $dates = [ 'created_at', 'updated_at', 'date_of_birth' ];


	/**
	 * @param $date
	 */
	public function setDateOfBirthAttribute( $date ) {
		$this->attributes['date_of_birth'] = Carbon::createFromFormat('d/m/Y', $date);
	}

	/**
	 * Display a person name
	 * @return string
	 */
	public function display_name() {
		return $this->first_name . ' ' . $this->last_name;
	}


	/**
	 * The memories that have this person tagged
	 */
	public function memories() {
		return $this->belongsToMany( 'App\Models\Memory' );
	}

	public function relationshiptypes(){
		return $this->hasOne('App\Models\RelationshipType', 'id', 'relationship_id');
	}

}
