<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemoryType extends Model {

	public $timestamps = false;
}
