<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group( [ 'middleware' => [ 'web' ] ], function () {

	//\Illuminate\Support\Facades\Session::flush();
	//dd(Session::get( 'facebook_access_token' ));

	Route::get( '/', function () {
		return view( 'pages.homepage' );
	} );

	// auth routes
	Route::auth();

	// memories routes
	Route::get( '/memories/facebook-import', 'MemoryController@importFacebook' );
	Route::get( '/memories/instagram-import', 'MemoryController@importInstagram' );
	Route::post( '/memories/upload', 'MemoryController@upload' );

	Route::post( '/memories/filter', 'MemoryController@filter' );
	Route::get( '/memoryfile/{file_name}', 'MemoryController@readFile' );

	Route::resource( 'memories', 'MemoryController' );


	// profile
	Route::get( '/profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit'] );
	Route::put( '/profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update'] );

	// trunk routes
	Route::resource( 'trunks', 'TrunkController' );
    // job routes
    Route::resource('jobs', 'JobController');

	// persons routes
	Route::resource( 'persons', 'PersonController' );

	// sharelinks link routes
	Route::get( 'sharelinks/s/{hash}', 'SharelinkController@hash');
	Route::get( 'sharelinks/read/{file_name}', 'SharelinkController@readFile' );
	Route::resource( 'sharelinks', 'SharelinkController');


} );


/*
|--------------------------------------------------------------------------
| API Endpoints
|--------------------------------------------------------------------------
|
| Endpoints are the similar to routes for an application
|
*/
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', ['middleware' => 'api.auth'], function ($api) {
	$api->get('users/{user_id}', 'App\Api\V1\Controllers\UserController@show');

	// trunks
	$api->get('users/{user_id}/trunks', 'App\Api\V1\Controllers\UserController@trunks');
	$api->post('users/{user_id}/trunks', 'App\Api\V1\Controllers\UserController@store_trunk');
	$api->put('users/{user_id}/trunks', 'App\Api\V1\Controllers\UserController@update_trunk');



	$api->get('users/{user_id}/memories', 'App\Api\V1\Controllers\UserController@memories');
	$api->get('users/{user_id}/persons', 'App\Api\V1\Controllers\UserController@persons');
	$api->get('users/{user_id}/jobs', 'App\Api\V1\Controllers\UserController@jobs');
});

// not requiring auth
$api->version('v1', function ($api) {
	$api->get('relationships/{relationship_id}', 'App\Api\V1\Controllers\RelationshipController@show');
});