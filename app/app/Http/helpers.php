<?php
function d_error($key, $errors){
	if(array_key_exists($key, $errors)){
		return '<span class="error">'. $errors[$key][0].'</span>';
	}
	return '';
}