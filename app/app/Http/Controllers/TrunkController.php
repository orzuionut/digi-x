<?php

namespace App\Http\Controllers;

use App\Models\Trunk;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class TrunkController extends Controller {


	public function __construct() {
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$user_id = Auth::id();
		$trunks = Trunk::where('user_id', $user_id)->get();

		return View::make( 'pages.trunks.index', compact('trunks'));
	}

	/**
	 * Show the form for creating a new resource.
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return View::make( 'pages.trunks.create' );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {
		$user_id = Auth::id();

		$this->validate( $request, [
			'title' => 'required|string',
			'description'  => 'string'
		] );


		$trunk = new Trunk();
		$trunk->title = $request->title;
		if($request->description){
			$trunk->description = $request->description;
		}

		$trunk->user_id = $user_id;
		$saved = $trunk->save();

		if($saved){
			$request->session()->flash('flash_message', 'Trunk was added!');
			$request->session()->flash('flash_message_type', 'success');
		} else {
			$request->session()->flash('flash_message', Config::get('strings.unknown_error'));
			$request->session()->flash('flash_message_type', 'error');
		}

		return Redirect::back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {
		$trunk = Trunk::find($id);

		return View::make( 'pages.trunks.edit', [
				'trunk' => $trunk
		] );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ) {
		$this->validate( $request, [
				'title' => 'required|string',
				'description'  => 'string'
		] );

		$user = Auth::user();
		if(!$user->is_trunk_owner($id)){
			$request->session()->flash('flash_message', Config::get('strings.unknown_error'));
			$request->session()->flash('flash_message_type', 'error');
			return Redirect::back();
		}

		$trunk = Trunk::find($id);
		$trunk->title = $request->title;
		$trunk->description = $request->description;
		$saved = $trunk->save();

		if($saved){
			$request->session()->flash('flash_message', 'Trunk was updated!');
			$request->session()->flash('flash_message_type', 'success');
		} else {
			$request->session()->flash('flash_message', Config::get('strings.unknown_error'));
			$request->session()->flash('flash_message_type', 'error');
		}

		return Redirect::back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {
		//
	}
}
