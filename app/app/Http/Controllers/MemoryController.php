<?php

namespace App\Http\Controllers;

use App\Digix\Memory\FacebookMemory;
use App\Digix\Memory\InstagramMemory;
use App\Http\Requests\MemoryUploadRequest;
use App\Models\Location;
use App\Models\Memory;
use App\Models\Job;
use App\Models\MemoryType;
use App\Models\Person;
use App\Models\RelationshipType;
use App\Models\Sharelink;
use App\Models\Trunk;
use App\Services\MemoryService;
use App\User;
use Facebook\Exceptions\FacebookSDKException;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Larabros\Elogram\Exceptions\APINotFoundError;
use Laracasts\Utilities\JavaScript;
use Illuminate\Support\Facades\Config;
use Larabros\Elogram\Exceptions\Exception;


class MemoryController extends Controller {

	private $memory_service;

	public function __construct() {
		$this->middleware( 'auth');
		$this->memory_service = new MemoryService();
	}

	/**
	 * Display a listing of the resource.
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$user     = Auth::user();
		$memories = $this->getAllMemoriesData( $user->id );
		$this->setMemoryBoxClass( $memories );


		$memory_types      = MemoryType::all();
		$jobs              = $user->jobs;
		$trunks            = $user->trunks;
		$persons           = $user->persons;
		$relationships     = RelationshipType::all();
		$locations         = $user->locations;
		$positions         = $user->positions;
		$companies         = $user->companies;
		$memories_ids_json = json_encode( $memories->pluck( 'id' ) );


		return View::make( 'pages.memories.index', [
			'memories'          => $memories,
			'memory_types'      => $memory_types,
			'jobs'              => $jobs,
			'trunks'            => $trunks,
			'persons'           => $persons,
			'relationships'     => $relationships,
			'locations'         => $locations,
			'positions'         => $positions,
			'companies'         => $companies,
			'memories_ids_json' => $memories_ids_json
		] );
	}

	/**
	 * Show the form for creating a new resource.
	 * @return \Illuminate\Http\Response
	 */
	public function create( Request $request ) {
		$user_id   = Auth::id();
		$fb        = new FacebookMemory( $user_id );
		$instagram = new InstagramMemory( $user_id );

		// get social memories if we have them and load them before getting the persons
		$facebook_memories  = Session::pull( 'facebook_memories' );
		$instagram_memories = Session::pull( 'instagram_memories' );
		$memories           = [ ];
		if ( $facebook_memories ) {
			$memories = $facebook_memories;
		} else if ( $instagram_memories ) {
			$memories = $instagram_memories;
		}


		// get user info
		$jobs           = Job::where( 'user_id', $user_id )->get();
		$persons        = Person::where( 'user_id', $user_id )->get();
		$trunks         = Trunk::where( 'user_id', $user_id )->get();
		$facebook_link  = $fb->getImportURL();
		$instagram_link = $instagram->getImportURL();


		return View::make( 'pages.memories.create', array(
			'jobs'           => $jobs,
			'trunks'         => $trunks,
			'persons'        => $persons,
			'facebook_link'  => $facebook_link,
			'instagram_link' => $instagram_link,
			'memories'       => $memories
		) );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {
		// validate
		$this->validate( $request, [
			'title'       => 'required|max:255',
			'file_name'   => 'required_without:source|max:255',
			'source'      => 'required_without:file_name|max:255',
			'trunk_id'    => 'required',
			'type_id'     => 'required',
			'memory_date' => 'required|date_format:d/m/Y'
		] );

		// get logged user and create memory instance
		$memory = new Memory;
		$user   = Auth::user();

		// set either local name or remote source
		if ( $request->file_name ) {
			$memory->file_name = $request->file_name;
		} else {
			$memory->source = $request->source;
		}

		$memory->type_id = $request->type_id;
		$response        = array();

		$memory = $this->checkAndSetAttributes( $memory, $request, $user );
		if ( $memory ) {
			$response = $this->checkAndSetPersons( $memory, $request, $user );
		}

		if ( $memory && $response ) {
			return response()->json( $response );
		} else {
			$response['general']['unknown_error'] = Config::get( 'strings.unknown_error' );

			return response()->json( $response, 422 );
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {
		$memory = Memory::find( $id );

		return View::make( 'pages.memories.show', [
			'memory' => $memory,
		] );
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {
		$user    = Auth::user();
		$user_id = $user->id;

		$memory  = Memory::find( $id );
		$jobs    = Job::where( 'user_id', $user_id )->get();
		$persons = Person::where( 'user_id', $user_id )->get();
		$trunks  = Trunk::where( 'user_id', $user_id )->get();

		return View::make( 'pages.memories.edit', [
			'memory'  => $memory,
			'jobs'    => $jobs,
			'trunks'  => $trunks,
			'persons' => $persons,
		] );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ) {
		$this->validate( $request, [
			'title'       => 'required|max:255',
			'trunk_id'    => 'required',
			'memory_date' => 'required|date_format:d/m/Y'
		] );

		// get logged user and create memory instance
		$memory   = Memory::find( $id );
		$user     = Auth::user();
		$response = array();

		$memory = $this->checkAndSetAttributes( $memory, $request, $user );
		if ( $memory ) {
			$response = $this->checkAndSetPersons( $memory, $request, $user );
		}

		if ( $memory && $response ) {
			$request->session()->flash( 'flash_message', 'Memory was updated!' );
			$request->session()->flash( 'flash_message_type', 'success' );
		} else {
			$request->session()->flash( 'flash_message', Config::get( 'strings.unknown_error' ) );
			$request->session()->flash( 'flash_message_type', 'error' );
		}

		return Redirect::back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id, Request $request ) {
		$memory   = Memory::find( $id );
		$success  = $memory->delete();
		$response = array();

		if ( $success ) {
			$request->session()->flash( 'flash_message', 'Memory was deleted!' );
			$request->session()->flash( 'flash_message_type', 'success' );
			$response['redirect_url'] = route( 'memories.index' );
		} else {
			$request->session()->flash( 'flash_message', Config::get( 'strings.unknown_error' ) );
			$request->session()->flash( 'flash_message_type', 'error' );
			$response['redirect_url'] = route( 'memories.show', $id );


		}

		return response()->json( $response );
	}


	/**
	 * Filter memories
	 *
	 * @param Request $request
	 */
	public function filter( Request $request ) {
		$response = $this->memory_service->filter( $request->all() );

		if ( ! empty( $response['errors'] ) ) {
			return response()->json( $response['errors'], 422 );
		} else {
			return response()->json( $response['memories_ids'] );
		}
	}

	/**
	 * Used by the plupload script
	 * @todo - store in temp folder, and then delete if the user doesn't complete
	 */
	public function upload( MemoryUploadRequest $request ) {
		$model = new Memory();

		// upload via url from social networks
		if ( $request->url ) {
			$response = $model->upload( $request->url );
		} else {
			$response = $model->upload();
		}

		if ( $response['success'] ) {
			return response()->json( $response );
		} else {
			return response()->json( $response, 422 );
		}
	}

	/**
	 * @return mixed
	 */
	public function importFacebook( Request $request ) {
		$user_id   = Auth::id();
		$fb_memory = new FacebookMemory( $user_id );

		try {
			$memories = $fb_memory->import();

			return redirect()->route( 'memories.create' )->with( 'facebook_memories', $memories );
		} catch( FacebookSDKException $e ) {
			$request->session()->flash( 'flash_message', 'We couldn\'t import from facebook due to the following reason: ' . $e->getMessage() );
			$request->session()->flash( 'flash_message_type', 'error' );

			return redirect()->route( 'memories.create' );
		}
	}

	/**
	 * @return mixed
	 */
	public function importInstagram( Request $request ) {
		$user_id          = Auth::id();
		$instagram_memory = new InstagramMemory( $user_id );

		try {
			$memories = $instagram_memory->import();

			return redirect()->route( 'memories.create' )->with( 'instagram_memories', $memories );
		} catch( Exception  $e ) {
			$request->session()->flash( 'flash_message', 'We couldn\'t import from instragram due to the following reason: ' . $e->getMessage() );
			$request->session()->flash( 'flash_message_type', 'error' );

			return redirect()->route( 'memories.create' );
		}
	}

	/**
	 * @param $memories
	 */
	private function setMemoryBoxClass( $memories ) {
		foreach ( $memories as $memory ) {
			switch ( $memory->type->name ) {
				case 'image':
					$memory->type->class = '';
					break;
				case 'video':
					$memory->type->class = 'fa-file-video-o';
					break;
				case 'document':
					$memory->type->class = 'fa-file-text';
					break;
				case 'audio':
					$memory->type->class = 'fa-file-audio-o';
					break;
				default:
					Log::warning( 'Invalid memory file type.' );
			}
		}
	}

	/**
	 * @param $user_id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	private function getAllMemoriesData( $user_id ) {
		$memories = Memory::with(
			'trunk',
			'user',
			'job',
			'location',
			'persons',
			'type'
		)->where( 'user_id', $user_id )->get();

		return $memories;
	}


	/**
	 * Reads file from folder outside root
	 * @param $memory_id
	 *
	 * @return mixed
	 */
	public function readFile( $memory_id ) {
		$memory     = Memory::find($memory_id);
		$file_path = $memory->get_file_path( $memory->file_name, $memory->user_id);

		$file = $file_path;
		if ( file_exists( $file ) ) {
			$finfo = finfo_open( FILEINFO_MIME_TYPE );
			$mime  = finfo_file( $finfo, $file );
			finfo_close( $finfo );
			header( "Content-Type: $mime" );
			readfile( $file );
		} else {
			return Response::make( null, 403 );
		}
	}

	/**
	 * Checks request and sets attributes
	 *
	 * @param Memory $memory
	 * @param Request $request
	 * @param User $user
	 *
	 * @return Memory|bool
	 */
	private function checkAndSetAttributes( Memory $memory, Request $request, User $user ) {
		// checks if is trunk owner before assign
		if ( ! $user->is_trunk_owner( $request->trunk_id ) ) {
			return false;
		}

		$memory->trunk_id    = $request->trunk_id;
		$memory->user_id     = $user->id;
		$memory->title       = $request->title;
		$memory->description = $request->description;

		// assign social id
		if ( $request->fb_id ) {
			$memory->fb_id = $request->fb_id;
		} else if ( $request->instagram_id ) {
			$memory->instagram_id = $request->instagram_id;
		}

		// create the location
		if ( $request->location_name && $request->location_data ) {
			$location_data = json_decode( $request->location_data, true );
			$location      = Location::handleCreate( $user->id, $request->location_name, $location_data );

			$memory->location_id = $location->id;
		}

		// set the job and date
		if ( $request->job_id ) {
			$memory->job_id = $request->job_id;
		}
		$memory->memory_date = $request->memory_date;

		// save memory
		$response = $memory->save();

		if ( $response ) {
			return $memory;
		} else {
			return false;
		}
	}

	/**
	 * @param Memory $memory
	 * @param Request $request
	 * @param User $user
	 *
	 * @return bool
	 */
	public function checkAndSetPersons( Memory $memory, Request $request, User $user ) {
		// after saving check if person exists/and belongs to current tenant
		if ( $request->persons ) {
			foreach ( $request->persons as $person_id ) {
				if ( ! $user->is_person_owner( $person_id ) ) {
					return false;
				};
			}
		} else {
			$request->persons = [ ]; // empty array
		}

		// sync the persons after creating the memory
		$memory->persons()->sync( $request->persons );

		return true;
	}
}
