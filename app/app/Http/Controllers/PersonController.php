<?php

namespace App\Http\Controllers;

use App\Models\RelationshipType;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Config;

use App\Models\Person;

class PersonController extends Controller {

	public function __construct() {
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$user_id = Auth::id();
		$persons = Person::where('user_id', $user_id)->get();
		
		return View::make( 'pages.persons.index', compact('persons'));
	}

	/**
	 * Show the form for creating a new resource.
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$relationships = RelationshipType::all();

		return View::make( 'pages.persons.create', [
			'relationships' => $relationships
		] );
	}

	/**
	 * Store a newly created resource in storage.
	 * @todo - check if relationship_id is valid
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {
		$user_id = Auth::id();

		$this->validate( $request, [
			'first_name'      => 'required|string',
			'last_name'       => 'required|string',
			'relationship_id' => 'required|integer'
		] );

		$person = new Person;
		$person->user_id = $user_id;
		$person->first_name = $request->first_name;
		$person->last_name = $request->last_name;
		$person->relationship_id = $request->relationship_id;
		if($request->date_of_birth){
			$person->date_of_birth = $request->date_of_birth;
		}
		$saved = $person->save();

		if($saved){
			$request->session()->flash('flash_message', 'Person was added!');
			$request->session()->flash('flash_message_type', 'success');
			//$response['redirect_url'] = action('PersonController@create');
			//return response()->json( $response );
		} else {
			$request->session()->flash('flash_message', Config::get('strings.unknown_error'));
			$request->session()->flash('flash_message_type', 'error');
		}

		return Redirect::back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show() {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $id ) {
		$person = Person::find($id);
		$relationships = RelationshipType::all();

		return View::make( 'pages.persons.edit', [
				'person' => $person,
				'relationships' => $relationships
		] );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ) {
		$this->validate( $request, [
				'first_name'      => 'required|string',
				'last_name'       => 'required|string',
				'relationship_id' => 'required|integer'
		] );

		$user = Auth::user();
		if(!$user->is_person_owner($id)){
			$request->session()->flash('flash_message', Config::get('strings.unknown_error'));
			$request->session()->flash('flash_message_type', 'error');
			return Redirect::back();
		};

		$person = Person::find($id);
		$person->first_name = $request->first_name;
		$person->last_name = $request->last_name;
		$person->relationship_id = $request->relationship_id;
		if($request->date_of_birth){
			$person->date_of_birth = $request->date_of_birth;
		}
		$saved = $person->save();

		if($saved){
			$request->session()->flash('flash_message', 'Person was updated!');
			$request->session()->flash('flash_message_type', 'success');
		} else {
			$request->session()->flash('flash_message', Config::get('strings.unknown_error'));
			$request->session()->flash('flash_message_type', 'error');
		}

//		return Redirect::back();
		return redirect('/persons');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {
		$person = Person::find($id);

		$destroyed = $person->delete();

		return redirect('/persons');
	}
}
