<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Job;
use App\Models\Position;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;

class JobController extends Controller
{


	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$user_id = Auth::id();
		$jobs = Job::where('user_id', $user_id)->get();

		return View::make('pages.jobs.index', compact('jobs'));
	}

	/**
	 * Show the form for creating a new resource.
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$user = Auth::user();
		$user_id = $user->id;

		$companies = Company::where('user_id', $user_id)->get();
		$positions = Position::where('user_id', $user_id)->get();

		return View::make('pages.jobs.create', [
			'companies' => $companies,
			'positions' => $positions
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'company_name' => 'string',
			'existing_company_id' => 'required_if:company_name,""|integer',
			'position_title' => 'string',
			'existing_position_id' => 'required_if:position_title,""|integer'
		]);

		$user = Auth::user();
		$user_id = $user->id;

		$company = $this->getCompany($request, $user_id);
		if($company == false){
			$response['general']['unknown_error'] = Config::get('strings.unknown_error');
			return response()->json($response, 422);
		}

		$position = $this->getPosition($request, $user_id);
		if($position == false){
			$response['general']['unknown_error'] = Config::get('strings.unknown_error');
			return response()->json($response, 422);
		}
		
		$job = new Job();
		$job->company_id = $company->id;
		$job->position_id = $position->id;
		$job->user_id = $user_id;
		$saved = $job->save();

		if ($saved) {
			$request->session()->flash('flash_message', 'Job was added!');
			$request->session()->flash('flash_message_type', 'success');
			$response['redirect_url'] = $request->url();
			return response()->json( $response );
		} else {
			$response['general']['unknown_error'] = Config::get('strings.unknown_error');
			return response()->json($response, 422);
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$user = Auth::user();
		$user_id = $user->id;

		$job = Job::find($id);

		$companies = Company::where('user_id', $user_id)->get();
		$positions = Position::where('user_id', $user_id)->get();

		return View::make('pages.jobs.edit', compact('job', 'companies', 'positions'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'company_name' => 'string',
			'existing_company_id' => 'required_if:company_name,""|integer',
			'position_title' => 'string',
			'existing_position_id' => 'required_if:position_title,""|integer'
		]);

		$user = Auth::user();
		$user_id = $user->id;

		if (!$user->is_job_owner($id)) {
			$request->session()->flash('flash_message', Config::get('strings.unknown_error'));
			$request->session()->flash('flash_message_type', 'error');
			return Redirect::back();
		}

		$company = $this->getCompany($request, $user_id);
		if($company == false){
			$response['general']['unknown_error'] = Config::get('strings.unknown_error');
			return response()->json($response, 422);
		}

		$position = $this->getPosition($request, $user_id);
		if($position == false){
			$response['general']['unknown_error'] = Config::get('strings.unknown_error');
			return response()->json($response, 422);
		}

		$job = Job::find($id);

		$job->company_id = $company->id;
		$job->position_id = $position->id;
		$saved = $job->save();

		if ($saved) {
			$request->session()->flash('flash_message', 'Job was updated!');
			$request->session()->flash('flash_message_type', 'success');
		} else {
			$request->session()->flash('flash_message', Config::get('strings.unknown_error'));
			$request->session()->flash('flash_message_type', 'error');
		}

		return Redirect('/jobs');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}

	public function getCompany(Request $request, $user_id)
	{
		if ($request->company_name) {
			$company = new Company();
			$company->name = $request->company_name;
			$company->user_id = $user_id;
			$company->save();
		} else {
			$company = Company::find($request->existing_company_id);
			if (!$company->is_owner($user_id)) {
				return false;
			}
		}

		return $company;
	}

	public function getPosition(Request $request, $user_id)
	{
		if ($request->position_title) {
			$position = new Position();
			$position->title = $request->position_title;
			$position->user_id = $user_id;
			$position->save();
		} else {
			$position = Position::find($request->existing_position_id);
			if (!$position->is_owner($user_id)) {
				return false;
			}
		}

		return $position;
	}
}
