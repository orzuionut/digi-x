<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class ProfileController extends Controller {

	/**
	 * @return mixed
	 */
	public function edit( ) {
		$user    = Auth::user();

		return View::make( 'pages.profile.edit', compact( 'user' ) );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 *
	 * @param Request $request
	 *
	 * @return mixed
	 */
	public function update( Request $request ) {
		$this->validate( $request, [
			'name'         => 'required|string|max:255',
		] );

		/** @var \App\User $user */
		$user    = Auth::user();
		$user->name = $request->name;
		$saved = $user->save();

		if ( $saved ) {
			$request->session()->flash( 'flash_message', 'Your profile was updated!' );
			$request->session()->flash( 'flash_message_type', 'success' );
		} else {
			$request->session()->flash( 'flash_message', Config::get( 'strings.unknown_error' ) );
			$request->session()->flash( 'flash_message_type', 'error' );
		}

		return back();
	}
}