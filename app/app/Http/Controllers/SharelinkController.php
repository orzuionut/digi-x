<?php

namespace App\Http\Controllers;

use App\Events\ShareLinkAccessed;
use App\Models\Memory;
use App\Models\Sharelink;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Http\Requests;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class SharelinkController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['hash', 'readFile']]);

    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::id();
        $sharelinks = Sharelink::where('user_id', $user_id)->get();

        return View::make('pages.sharelinks.index', compact('sharelinks'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $user_id = Auth::id();
        $memories = Memory::where('user_id', $user_id)->get();

        return View::make('pages.sharelinks.create', compact('memories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = Auth::id();

        $this->validate($request, [
            'memory_id' => 'required',
        ]);

        $sharelink = new Sharelink();
        $sharelink->memory_id = $request->memory_id;

        $sharelink->user_id = $user_id;

        $string = $user_id . $request->memory_id . uniqid();

        $hashed_link = md5($string);

        $sharelink->hashed_link = $hashed_link;

        $saved = $sharelink->save();

        if ($saved) {
            $request->session()->flash('flash_message', 'Sharelink was created!');
            $request->session()->flash('flash_message_type', 'success');
        } else {
            $request->session()->flash('flash_message', Config::get('strings.unknown_error'));
            $request->session()->flash('flash_message_type', 'error');
        }

        return Redirect::back();
    }

    public function hash($hash)
    {
        $sharelink = Sharelink::where('hashed_link', $hash)->first();

        if ($sharelink) {
            $generated_at = $sharelink->created_at;
            $alive_link_time = $generated_at->addDay(1);
            $time_right_now = Carbon::now();
            $time_passed = $time_right_now->diffInSeconds($alive_link_time);

            if ($time_passed > 86400) {
                return Response::make(null, 410);
            }

            Event::fire(new ShareLinkAccessed($sharelink));

            return View::make( 'pages.sharelinks.show', [
                'memory' => $sharelink->memory,
                'hash' => $sharelink->hashed_link,
            ] );
        } else {
            return Response::make(null, 404); //should be 404 or custom not 403
        }
    }

    public function destroy($id)
    {

        $sharelink = Sharelink::where('id', $id);
        $sharelink->delete();

        return Redirect::back();
    }

    /**
     * Reads file from folder outside root
     * @param $hash
     *
     * @return mixed
     */
    public function readFile( $hash ) {
        $sharelink = Sharelink::where('hashed_link', $hash)->first();
        $memory = $sharelink->memory;

        $file_path = $memory->get_file_path( $memory->file_name, $memory->user_id);

        $file = $file_path;
        if ( file_exists( $file ) ) {
            $finfo = finfo_open( FILEINFO_MIME_TYPE );
            $mime  = finfo_file( $finfo, $file );
            finfo_close( $finfo );
            header( "Content-Type: $mime" );
            readfile( $file );
        } else {
            return Response::make( null, 403 );
        }
    }

}
