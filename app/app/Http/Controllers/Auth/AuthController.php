<?php

namespace App\Http\Controllers\Auth;

use App\Models\Trunk;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller {
	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers, ThrottlesLogins;

	/**
	 * Where to redirect users after login / registration.
	 * @var string
	 */
	protected $redirectTo = '/memories';
	protected $loginPath = '/login';
	protected $registerView = 'pages.register';
	protected $loginView = 'pages.login';

	/**
	 * Create a new authentication controller instance.
	 * @return void
	 */
	public function __construct() {
		$this->middleware( 'guest', [ 'except' => 'logout' ] );
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array $data
	 *
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator( array $data ) {
		return Validator::make( $data, [
			'name'     => 'required|max:255',
			'email'    => 'required|email|max:255|unique:users',
			'password' => 'required|confirmed|min:6',
		] );
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array $data
	 *
	 * @return User
	 */
	protected function create( array $data ) {
		$api_token = str_random( 40 );
		while( User::where( 'api_token', $api_token )->first() ) {
			$api_token = str_random( 40 );
		}

		$user = new User();
		$user->name = $data['name'];
		$user->email = $data['email'];
		$user->password = bcrypt( $data['password'] );
		$user->api_token = $api_token;
		$user->save();


		// create the default trunk
		$trunk = new Trunk();
		$trunk->title = "Uncategorized";
		$trunk->user_id = $user->id;
		$trunk->save();



		return $user;
	}
}
