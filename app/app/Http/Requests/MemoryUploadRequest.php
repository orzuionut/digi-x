<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class MemoryUploadRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 * @return bool
	 */
	public function authorize() {
		$user_id = Auth::id();
		if ( $user_id ) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Get the validation rules that apply to the request.
	 * @return array
	 */
	public function rules() {
		$image_mime_types = Config::get( 'memory.validation.image_mime_types' );
		$video_mime_types = Config::get( 'memory.validation.video_mime_types' );
		$doc_mime_types   = Config::get( 'memory.validation.doc_mime_types' );
		$audio_mime_types = Config::get( 'memory.validation.audio_mime_types' );
		$max_size         = Config::get( 'memory.validation.max_size' );

		return [
			'file' => array(
				'max:' . $max_size,
				'mimes:' . implode( ',', array_merge( $image_mime_types, $video_mime_types, $doc_mime_types, $audio_mime_types ) )
			)
		];
	}

	public function messages() {
		$image_mime_types = Config::get( 'memory.validation.image_mime_types' );
		$video_mime_types = Config::get( 'memory.validation.video_mime_types' );
		$doc_mime_types   = Config::get( 'memory.validation.doc_mime_types' );
		$audio_mime_types = Config::get( 'memory.validation.audio_mime_types' );

		return [
			'file.mimes:' . implode( ',', array_merge( $image_mime_types, $video_mime_types, $doc_mime_types, $audio_mime_types ) ) => 'Invalid mimetype.'
		];
	}
}
