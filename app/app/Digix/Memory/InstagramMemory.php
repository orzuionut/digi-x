<?php namespace App\Digix\Memory;

use App\Digix\File;
use App\Models\Location;
use App\Models\Memory;
use App\Models\MemoryType;
use App\Models\Person;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class InstagramMemory extends SocialMemory {

	private $instagram;

	/**
	 * FacebookMemoryService constructor.
	 *
	 * @param $user_id
	 */
	public function __construct( $user_id ) {
		parent::__construct( $user_id );

		// the instagram package needs native sessions
		// @see - http://elogram.readthedocs.io/en/stable/developers.html to implement
		if ( ! isset( $_SESSION ) ) {
			session_start();
		}

		$this->existing_memories = Memory::where('user_id', $user_id)->pluck( 'instagram_id' );
		$this->import_action = 'MemoryController@importInstagram';
		$this->instagram = App::make( 'Larabros\Elogram\Client' );
	}

	/**
	 * Instagram login link
	 * @return mixed
	 */
	public function getLoginURL() {
		$options = [ 'scope' => 'basic public_content' ];

		return $this->instagram->getLoginUrl( $options );
	}

	/**
	 * Sets token if we have the code, or redirects
	 * in order to get the code
	 *
	 * @return bool
	 */
	public function setToken() {
		if ( !empty($_GET['code']) ) {
			$token = $this->instagram->getAccessToken( $_GET['code'] );
			Session::put( 'instagram_access_token', $token );
			return $token;
		} else {
			return redirect($this->getLoginURL());
		}
	}

	/**
	 * Returns existing token
	 *
	 * @return mixed
	 */
	public function getToken(){
		return Session::get( 'instagram_access_token');
	}

	/**
	 * Gets all memories
	 * @return array|bool
	 */
	public function getMemories() {
		$token = $this->getToken();

		// we are creating the object again, as there is a issue with setAccessToken
		// the method sets the token as json, but in
		// vendor/larabros/elogram/src/Http/Middleware/AuthMiddleware.php line 36 - current version is v.1.2.2
		// we are expecting an object and as such, we are
		// getting Call to a member function getToken() on a non-object
		// @todo - dig deeper, as I haven't looked into the middleware section that transforms the JSON into
		// object again vendor/larabros/elogram/src/Http/Providers/MiddlewareServiceProvider.php line 56
		$this->instagram = App::make( 'Larabros\Elogram\Client', array(json_encode($token->jsonSerialize())));
		//$this->instagram->setAccessToken( $token );

		$response = $this->instagram->request( 'GET', 'users/self/media/recent' );

		$memories = $this->parseResponse( $response );
		return $memories;
	}

	/**
	 * Parses facebook response after querying images
	 *
	 * @param $response
	 *
	 * @return array
	 */
	public function parseResponse( $response ) {
		$data = $response->get();
		if ( $data->isEmpty() ) {
			return false;
		}

		$this->social_name = $data[0]['user']['full_name'];
		$memories          = array();

		// initial batch
		$data = $response->get();
		if($data){
			foreach ( $data as $ig_memory ) {
				$memory     = $this->parseFields( $ig_memory, $ig_memory['type'] );
				$memories[] = $memory;
			}
		}

		// next pages
		while($response->hasPages()){
			$response = $this->instagram->request( 'GET', $response->nextUrl() );
			$data = $response->get();
			foreach ( $data as $ig_memory ) {
				$memory     = $this->parseFields( $ig_memory, $ig_memory['type'] );
				$memories[] = $memory;
			}
		}

		return $memories;
	}

	/**
	 * Parses fields
	 *
	 * @param $data
	 *
	 * @return array
	 */
	protected function parseFields( $data, $memory_type ) {
		$memory                 = new \stdClass();
		$memory->instagram_id   = $data['id'];
		$memory->social_html_id = 'insta_' . $memory->instagram_id;
		$memory->social_type = 'instagram';

		$memory->notes = '';
		if ( $this->existing_memories->contains( $memory->instagram_id ) ) {
			$memory->notes = '(memory was imported already)';
		}

		// set memory source
		if ( $memory_type == 'image' ) {
			$memory->source = $data['images']['standard_resolution']['url']; // get the biggest image
		} else {
			$memory->source = $data['videos']['standard_resolution']['url'];
		}


		// set memory title
		$file          = new File( $memory->source, true );
		$memory->title = ! empty( $data['caption'] ) ? $data['caption']['text'] : $file->getFileName();


		// type & date
		$memory_type         = MemoryType::where( 'name', $memory_type )->first();
		$memory->type_id     = $memory_type->id;
		$memory->type_name   = $memory_type->name;
		$memory->memory_date = Carbon::createFromTimestamp( $data['created_time'] )->format( 'd/m/Y' );

		// location
		if ( ! empty( $data['location'] ) ) {
			$memory->location = $this->handleLocation( $data['location'] );
		}

		// persons
		if ( ! empty( $data['tags'] ) ) {
			$memory->persons = $this->handlePersons( $data['tags'] );
		}

		return $memory;
	}


	/**
	 * Parses facebook data and creates location for
	 * the user
	 *
	 * @param $location
	 *
	 * @return static
	 */
	protected function handleLocation( $location ) {
		$if_location = $location;
		$data        = array(
			'city'      => ! empty( $if_location['city'] ) ? $if_location['city'] : null,
			'country'   => ! empty( $if_location['country'] ) ? $if_location['country'] : null,
			'latitude'  => ! empty( $if_location['latitude'] ) ? $if_location['latitude'] : null,
			'longitude' => ! empty( $if_location['longitude'] ) ? $if_location['longitude'] : null,
			'street'    => ! empty( $if_location['street'] ) ? $if_location['street'] : null,
			'zip'       => ! empty( $if_location['zip'] ) ? $if_location['zip'] : null
		);

		$name     = ! empty( $location['name'] ) ? $location['name'] : '';
		$location = Location::handleCreate( $this->user_id, $name, $data );

		return $location;
	}

	/**
	 * Creates persons from the tags on the images
	 *
	 * @param $tags
	 *
	 * @return array
	 */
	protected function handlePersons( $tags ) {
		$persons = array();
		foreach ( $tags as $key => $tag ) {
			if ( $this->social_name == $tag['name'] ) { // this is our user
				continue;
			}

			$parts     = explode( " ", $tag['name'] );
			$lastname  = array_pop( $parts );
			$firstname = implode( " ", $parts );

			$existing_person = Person::where( 'first_name', $firstname )->where( 'last_name', $lastname )->get()->first();

			if ( $existing_person ) {
				$person = $existing_person;
			} else {
				$person                  = new Person();
				$person->first_name      = $firstname;
				$person->last_name       = $lastname;
				$person->user_id         = $this->user_id;
				$person->relationship_id = $this->default_relationship_id;
				$person->save();
			}

			// update facebook ID either way
			if ( ! empty( $tag['id'] ) ) {
				$person->facebook_id = $tag['id'];
			}
			$person->save();

			$persons[ $key ] = $person;
		}

		return $persons;
	}

	/**
	 * Creates a unique filename
	 * @todo - check if splitting by ? is alright - i.e. if fb
	 * does't use ? in the filename
	 *
	 * @param $url
	 *
	 * @return string
	 */
	private function createUniqueFilename( $url ) {
		$split     = explode( "/", $url );
		$file_name = array_pop( $split );

		$split     = explode( "?", $file_name );
		$file_name = $split[0];

		$memory = new Memory();

		$file_name = $memory->get_unique_filename( $file_name, $this->user_id );

		return $file_name;
	}
}
