<?php namespace App\Digix\Memory;

use App\Digix\File;
use App\Models\Location;
use App\Models\Memory;
use App\Models\MemoryType;
use App\Models\Person;
use Facebook\FacebookResponse;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class FacebookMemory extends SocialMemory {

	private $fb;

	/**
	 * FacebookMemoryService constructor.
	 *
	 * @param $user_id
	 */
	public function __construct( $user_id ) {
		parent::__construct( $user_id );

		$this->existing_memories = Memory::where( 'user_id', $user_id )->pluck( 'fb_id' );
		$this->import_action     = 'MemoryController@importFacebook';
		$this->fb                = App::make( 'SammyK\LaravelFacebookSdk\LaravelFacebookSdk' );
	}

	/**
	 * Returns login url to request permissions
	 * @return mixed
	 */
	public function getLoginURL() {
		return $this->fb->getLoginUrl( [
			'email',
			'user_status',
			'user_posts',
			'user_photos',
			'user_videos',
			'user_location',
			'user_hometown'
		], action( $this->import_action ) );
	}

	/**
	 * Sets facebook token if we have it,
	 * or redirects to facebook to get it
	 * @return bool
	 */
	public function setToken() {
		$token = $this->fb
			->getRedirectLoginHelper()
			->getAccessToken();

		if ( ! $token ) {
			return redirect( $this->getLoginURL() );
		} else {
			Session::put( 'facebook_access_token', (string) $token );

			return $token;
		}
	}

	/**
	 * Returns existing token
	 * @return mixed
	 */
	public function getToken() {
		// for testing purposes
		//return Session::pull( 'facebook_access_token' );
		return Session::get( 'facebook_access_token' );
	}

	/**
	 * Gets all memories
	 * @return array|bool
	 */
	public function getMemories() {
		$token = $this->getToken();
		$this->fb->setDefaultAccessToken( $token );

		$response = $this->fb->get( 'me/?fields=name,photos.limit(20){id,name,created_time,images,place{name,location{name,country,country_code,state,city,street,zip,longitude,latitude}},tags{id,name}},videos.limit(20){id,title,created_time,source,place,tags}' );

		$memories = $this->parseResponse( $response );

		return $memories;
	}

	/**
	 * Parses facebook response after querying images
	 *
	 * @param FacebookResponse $response
	 *
	 * @return array
	 */
	public function parseResponse( $response ) {
		$data              = $response->getGraphNode();
		$this->social_name = $data->getField( 'name' );
		$fb_photos         = $data->getField( 'photos' );
		$fb_videos         = $data->getField( 'videos' );
		$memories          = array();

		// the photos
		if ( $fb_photos ) {
			do {

				foreach ( $fb_photos as $fb_photo ) {
					$memory     = $this->parseFields( $fb_photo, 'image' );
					$memories[] = $memory;
				}

			} while( $fb_photos = $this->fb->next( $fb_photos ) );
		}

		// the videos
		if ( $fb_videos ) {
			do {
				foreach ( $fb_videos as $fb_video ) {
					$memory     = $this->parseFields( $fb_video, 'video' );
					$memories[] = $memory;
				}

			} while( $fb_videos = $this->fb->next( $fb_videos ) );
		}

		return $memories;
	}

	/**
	 * Parses fields
	 *
	 * @param $data
	 *
	 * @return array
	 */
	protected function parseFields( $data, $memory_type ) {
		$memory                 = new \stdClass();
		$memory->fb_id          = $data['id'];
		$memory->social_html_id = 'fb_' . $memory->fb_id;
		$memory->social_type    = 'facebook';

		$memory->notes = '';
		if ( $this->existing_memories->contains( $memory->fb_id ) ) {
			$memory->notes = '(memory was imported already)';
		}

		// set memory source
		if ( $memory_type == 'image' ) {
			$memory->source = $data['images'][0]['source']; // get the biggest image
		} else {
			$memory->source = $data['source'];
		}

		// set memory title
		$file          = new File( $memory->source, true );
		$memory->title = ! empty( $data['name'] ) ? $data['name'] : $file->getFileName();


		// type & date
		$memory_type         = MemoryType::where( 'name', $memory_type )->first();
		$memory->type_id     = $memory_type->id;
		$memory->type_name   = $memory_type->name;
		$memory->memory_date = $data['created_time']->format( 'd/m/Y' );

		// location
		if ( ! empty( $data['place'] ) && ! empty( $data['place']['location'] ) ) {
			$memory->location = $this->handleLocation( $data['place'] );
		}

		// persons
		if ( ! empty( $data['tags'] ) ) {
			$memory->persons = $this->handlePersons( $data['tags'] );
		}

		return $memory;
	}


	/**
	 * Parses facebook data and creates location for
	 * the user
	 *
	 * @param $place
	 *
	 * @return static
	 */
	protected function handleLocation( $place ) {
		$fb_location = $place['location'];
		$data        = array(
			'city'      => ! empty( $fb_location['city'] ) ? $fb_location['city'] : null,
			'country'   => ! empty( $fb_location['country'] ) ? $fb_location['country'] : null,
			'latitude'  => ! empty( $fb_location['latitude'] ) ? $fb_location['latitude'] : null,
			'longitude' => ! empty( $fb_location['longitude'] ) ? $fb_location['longitude'] : null,
			'street'    => ! empty( $fb_location['street'] ) ? $fb_location['street'] : null,
			'zip'       => ! empty( $fb_location['zip'] ) ? $fb_location['zip'] : null
		);

		$name     = ! empty( $place['name'] ) ? $place['name'] : '';
		$location = Location::handleCreate( $this->user_id, $name, $data );

		return $location;
	}

	/**
	 * Creates persons from the tags on the images
	 *
	 * @param $tags
	 *
	 * @return array
	 */
	protected function handlePersons( $tags ) {
		$persons = array();
		foreach ( $tags as $key => $tag ) {
			if ( $this->social_name == $tag['name'] ) { // this is our user
				continue;
			}

			$parts     = explode( " ", $tag['name'] );
			$lastname  = array_pop( $parts );
			$firstname = implode( " ", $parts );

			$existing_person = Person::where( 'first_name', $firstname )->where( 'last_name', $lastname )->get()->first();

			if ( $existing_person ) {
				$person = $existing_person;
			} else {
				$person                  = new Person();
				$person->first_name      = $firstname;
				$person->last_name       = $lastname;
				$person->user_id         = $this->user_id;
				$person->relationship_id = $this->default_relationship_id;
				$person->save();
			}

			// update facebook ID either way
			if ( ! empty( $tag['id'] ) ) {
				$person->facebook_id = $tag['id'];
			}
			$person->save();

			$persons[ $key ] = $person;
		}

		return $persons;
	}

	/**
	 * Creates a unique filename
	 * @todo - check if splitting by ? is alright - i.e. if fb
	 * does't use ? in the filename
	 *
	 * @param $url
	 *
	 * @return string
	 */
	private function createUniqueFilename( $url ) {
		$split     = explode( "/", $url );
		$file_name = array_pop( $split );

		$split     = explode( "?", $file_name );
		$file_name = $split[0];

		$memory = new Memory();

		$file_name = $memory->get_unique_filename( $file_name, $this->user_id );

		return $file_name;
	}
}
