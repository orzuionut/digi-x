<?php namespace App\Digix\Memory;


use App\Models\Location;
use App\Models\MemoryType;
use App\Models\Person;
use App\Models\RelationshipType;

abstract class SocialMemory {

	protected $user_id;
	protected $default_relationship_id;
	protected $social_name;
	protected $existing_memories;
	protected $import_action;

	public function __construct( $user_id ) {
		$this->user_id                 = $user_id;
		$this->default_relationship_id = RelationshipType::find( '1' )->id;
	}

	/**
	 * Checks to see if we have the token
	 * if we don't we set it by redirecting to the social network link
	 *
	 * @return mixed
	 */
	public function import(){
		if(!$this->getToken()){
			$this->setToken();
		}

		return $this->getMemories();
	}

	/**
	 * We return our app import url if we already have the
	 * permissions, or instagram login link
	 *
	 * @return mixed|string
	 */
	public function getImportURL(){
		if($this->getToken()){
			return action($this->import_action);
		} else {
			return $this->getLoginURL();
		}
	}

	/**
	 * @return mixed
	 */
	abstract public function getLoginURL();

	/**
	 * Sets the OAuth token
	 *
	 * @return mixed
	 */
	abstract public function setToken();


	/**
	 * Returns the OAuth token
	 *
	 * @return mixed
	 */
	abstract public function getToken();


	/**
	 * Performs the request and gets the memories
	 *
	 * @return mixed
	 */
	abstract public function getMemories();

	/**
	 * Parses response
	 *
	 * @param $response
	 *
	 * @return mixed
	 */
	abstract protected function parseResponse($response);

	/**
	 * Parses fields for a social memory
	 *
	 * @param $data
	 * @param $memory_type
	 *
	 * @return mixed
	 */
	abstract protected function parseFields($data, $memory_type);


	/**
	 * Parses facebook data and creates location for
	 * the user
	 *
	 * @param $place
	 *
	 * @return Location
	 */
	protected function handleLocation( $place ) {
		$fb_location = $place['location'];
		$data        = array(
				'city'      => ! empty( $fb_location['city'] ) ? $fb_location['city'] : null,
				'country'   => ! empty( $fb_location['country'] ) ? $fb_location['country'] : null,
				'latitude'  => ! empty( $fb_location['latitude'] ) ? $fb_location['latitude'] : null,
				'longitude' => ! empty( $fb_location['longitude'] ) ? $fb_location['longitude'] : null,
				'street'    => ! empty( $fb_location['street'] ) ? $fb_location['street'] : null,
				'zip'       => ! empty( $fb_location['zip'] ) ? $fb_location['zip'] : null
		);


		$name = !empty($place['name']) ? $place['name'] : '';
		$location = Location::handleCreate( $this->user_id, $name, $data );

		return $location;
	}


	/**
	 * Creates persons from the tags on the images
	 *
	 * @param $tags
	 *
	 * @return Person[]
	 */
	protected function handlePersons( $tags ) {
		$persons = array();
		foreach ( $tags as $key => $tag ) {
			if($this->fb_user_name == $tag['name']){ // this is our user
				continue;
			}

			$parts     = explode( " ", $tag['name'] );
			$lastname  = array_pop( $parts );
			$firstname = implode( " ", $parts );

			$existing_person = Person::where( 'first_name', $firstname )->where( 'last_name', $lastname )->get()->first();

			if ( $existing_person ) {
				$person = $existing_person;
			} else {
				$person                  = new Person();
				$person->first_name      = $firstname;
				$person->last_name       = $lastname;
				$person->user_id         = $this->user_id;
				$person->relationship_id = $this->default_relationship_id;
				$person->save();
			}

			// update facebook ID either way
			if(!empty($tag['id'])){
				$person->facebook_id = $tag['id'];
			}
			$person->save();

			$persons[ $key ] = $person;
		}

		return $persons;
	}
}