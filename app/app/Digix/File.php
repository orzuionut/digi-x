<?php
/**
 * Created by PhpStorm.
 * User: cristian
 * Date: 27.05.2016
 * Time: 12:22
 */

namespace App\Digix;


use Ixudra\Curl\Facades\Curl;

class File {
	private $url;
	private $file;

	/**
	 * File constructor.
	 *
	 * accepts either a file or url
	 *
	 * @param $file_or_url
	 * @param $with_url
	 */
	public function __construct($file_or_url, $with_url = false) {
		if($with_url){
			$this->url = $file_or_url;
			$this->file = Curl::to($this->url)->enableDebug(storage_path('logFile.txt'));
		} else {
			$this->file = $file_or_url;
		}
	}

	/**
	 * Gets file name
	 *
	 * @return mixed
	 */
	public function getFileName(){
		if($this->url){
			return $this->getFileNameFromURL();
		} else {
			return $this->file->getClientOriginalName();
		}
	}

	/**
	 * Gets file extension
	 *
	 * @return mixed
	 */
	public function getFileExt(){
		if($this->url){
			return $this->getFileExtFromURL();
		} else {
			return $this->file->getClientOriginalExtension();
		}
	}

	public function move($destinationPath, $fileName){
		if($this->url){
			$response = $this->file->download($destinationPath.$fileName);
			if(!$response){
				throw new \Exception('Couldn\'t fetch file from the URL.');
			}
		} else {
			$this->file->move($destinationPath, $fileName);
		}
	}

	/**
	 * Gets file name from url
	 *
	 * @return mixed
	 */
	private function getFileNameFromURL(){
		$split = explode("/", $this->url);
		$file_name = array_pop($split);

		$split = explode("?", $file_name);
		$file_name = $split[0];

		return $file_name;
	}

	/**
	 * Gets file extension
	 *
	 * @return mixed
	 */
	private function getFileExtFromURL(){
		$file_name = $this->getFileNameFromURL();
		$ext = pathinfo($file_name, PATHINFO_EXTENSION);

		return $ext;
	}
}