<?php

namespace App\Listeners;

use App\Events\ShareLinkAccessed;
use App\Models\Memory;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class NotifyShareLinkAccess {
	/**
	 * Create the event listener.
	 * @return void
	 */
	public function __construct() {
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  ShareLinkAccessed $event
	 *
	 * @return void
	 */
	public function handle( ShareLinkAccessed $event ) {
		$sharelink = $event->sharelink;
		$memory    = Memory::find( $sharelink->memory_id );
		$user      = $memory->user;

		Mail::send( 'emails.sharelink-accessed', [ 'memory' => $memory, 'user' => $user ], function ( $m ) use ( $user ) {
			$m->from( 'digi-x@dev.com', 'DIGI-X' );

			$m->to( $user->email, $user->name )->subject( 'Your shared link was accessed!' );
		} );
	}
}
