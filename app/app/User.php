<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'api_token', 'remember_token',
    ];

    /**
     * Returns list of trunks
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function memories(){
        return $this->hasMany('App\Models\Memory');
    }


    /**
     * Returns list of trunks
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function trunks(){
        return $this->hasMany('App\Models\Trunk');
    }

    /**
     * Returns list of persons
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function persons(){
        return $this->hasMany('App\Models\Person');
    }

    /**
     * Returns list of jobs
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jobs(){
        return $this->hasMany('App\Models\Job');
    }

    /**
     * Returns list of companies
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function companies(){
        return $this->hasMany('App\Models\Company');
    }

    /**
     * Returns list of positions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function positions(){
        return $this->hasMany('App\Models\Position');
    }


    /**
     * Returns user locations
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function locations(){
        return $this->hasMany('App\Models\Location');
    }

    /**
     * Return user sharelinks
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sharelinks(){
        return $this->hasMany('App\Models\Sharelink');
    }

    /**
     * Checks if user is owner of a trunk
     *
     * @param $trunk_id
     *
     * @return bool
     */
    public function is_trunk_owner($trunk_id){
        $trunks = $this->trunks->pluck('id');

        return in_array($trunk_id, $trunks->toArray());
    }



    /**
     * Checks if user is owner of a person
     *
     * @param $person_id
     *
     * @return bool
     */
    public function is_person_owner($person_id){
        $persons = $this->persons->pluck('id');

        return in_array($person_id, $persons->toArray());
    }
}
