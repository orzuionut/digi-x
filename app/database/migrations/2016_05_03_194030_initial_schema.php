<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialSchema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->unsignedInteger('user_id');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('positions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 255);
            $table->unsignedInteger('user_id');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('company_id');
            $table->unsignedInteger('position_id');
            $table->unsignedInteger('user_id');
            $table->timestamps();

            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('position_id')->references('id')->on('positions');
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('name', 255)->nullable();
            $table->string('country', 255)->nullable();
            $table->string('country_code', 3)->nullable();
            $table->string('city', 255)->nullable();
            $table->string('state', 255)->nullable();
            $table->string('street', 255)->nullable();
            $table->string('zip', 255)->nullable();
            $table->decimal('latitude', 9, 6);
            $table->decimal('longitude', 9, 6);

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('relationship_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->unsignedInteger('degree');
        });

        Schema::create('persons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 255);
            $table->string('last_name', 255);
            $table->date('date_of_birth')->nullable();
            $table->unsignedInteger('relationship_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('facebook_id')->nullable();
            $table->timestamps();

            $table->foreign('relationship_id')->references('id')->on('relationship_types');
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('trunks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 255);
            $table->string('description', 255);
            $table->unsignedInteger('user_id');
            $table->binary('meta_data')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::create('memory_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
        });

        Schema::create('memories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 255);
            $table->string('file_name', 255)->nullable(); // we can either have file_name or source
            $table->string('source', 255)->nullable();
            $table->bigInteger('fb_id')->unsigned()->nullable();
            $table->string('instagram_id', 255)->nullable();
            $table->text('description');
            $table->unsignedInteger('trunk_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('type_id');
            $table->unsignedInteger('location_id')->nullable();
            $table->unsignedInteger('job_id')->nullable();
            $table->date('memory_date')->nullable();
            $table->binary('meta_data')->nullable();
            $table->softDeletes();
            $table->timestamps();


            $table->foreign('trunk_id')->references('id')->on('trunks');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('type_id')->references('id')->on('memory_types');
            $table->foreign('location_id')->references('id')->on('locations');
            $table->foreign('job_id')->references('id')->on('jobs');
        });

        // used as pivot table
        // table is derived from the alphabetical order of the related model names
        Schema::create('memory_person', function (Blueprint $table) {
            $table->unsignedInteger('memory_id');
            $table->unsignedInteger('person_id');
            $table->primary(['memory_id', 'person_id']);

            $table->foreign('memory_id')->references('id')->on('memories');
            $table->foreign('person_id')->references('id')->on('persons');
        });

        Schema::create('sharelinks', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('memory_id');
            $table->unsignedInteger('user_id');
            $table->binary('hashed_link');
            $table->timestamps();

            $table->foreign('memory_id')->references('id')->on('memories');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sharelinks');
        Schema::dropIfExists('memory_person');
        Schema::dropIfExists('memories');
        Schema::dropIfExists('memory_types');
        Schema::dropIfExists('trunks');
        Schema::dropIfExists('persons');
        Schema::dropIfExists('relationship_types');
        Schema::dropIfExists('locations');
        Schema::dropIfExists('jobs');
        Schema::dropIfExists('positions');
        Schema::dropIfExists('companies');
    }
}
