<?php

use App\User;
use Illuminate\Database\Seeder;

class DummyDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $api_token = str_random( 40 );
        while( User::where( 'api_token', $api_token )->first() ) {
            $api_token = str_random( 40 );
        }

        DB::table('users')->insert([
            [
                'name' => 'John Doe',
                'email' => 'john@doe.com',
                'password' => Hash::make('1q2w3e4r5t'),
                'api_token' => $api_token,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]
        ]);

        DB::table('trunks')->insert([
            [
                'title' => 'american experience',
                'user_id' => '1',
            ],
            [
                'title' => 'camping',
                'user_id' => '1',
            ],
            [
                'title' => 'Our wedding',
                'user_id' => '1',
            ],
            [
                'title' => 'family time ',
                'user_id' => '1',
            ],
            [
                'title' => 'other stuff',
                'user_id' => '1',
            ]
        ]);

        DB::table('locations')->insert([
            [
                'name' => 'Copou, Iași, Județul Iași, România',
                'user_id' => '1',
                'country' => 'Romania',
                'country_code' => 'RO',
                'country' => 'Romania',
                'city' => 'Iași',
                'latitude' => '47.180215',
                'longitude' => '27.568450'
            ]
        ]);

        DB::table('positions')->insert([
            [
                'title' => 'Senior programmer',
                'user_id' => '1',
            ],
            [
                'title' => 'Senior systems architect',
                'user_id' => '1',
            ],
            [
                'title' => 'Head of business development',
                'user_id' => '1',
            ],
        ]);

        DB::table('companies')->insert([
            [
                'name' => 'Amazon',
                'user_id' => '1',
            ],
            [
                'name' => 'Salesforce',
                'user_id' => '1',
            ]
        ]);

        DB::table('jobs')->insert([
            [
                'company_id' => '1',
                'position_id' => '1',
                'user_id' => '1',
            ],
            [
                'company_id' => '2',
                'position_id' => '3',
                'user_id' => '1',
            ],
        ]);

        DB::table('persons')->insert([
            [
                'first_name' => 'Amanda',
                'last_name' => 'Doe',
                'date_of_birth' => \Carbon\Carbon::createFromDate(1989,07,22),
                'relationship_id' => '7',
                'user_id' => '1',
            ],
            [
                'first_name' => 'Mike',
                'last_name' => 'Doe',
                'date_of_birth' => \Carbon\Carbon::createFromDate(2008,07,30),
                'relationship_id' => '4',
                'user_id' => '1',
            ],
            [
                'first_name' => 'Andrea',
                'last_name' => 'Doe',
                'date_of_birth' => \Carbon\Carbon::createFromDate(2010,02,11),
                'relationship_id' => '5',
                'user_id' => '1',
            ],
            [
                'first_name' => 'Jason',
                'last_name' => 'Doe',
                'date_of_birth' => \Carbon\Carbon::createFromDate(1955,01,20),
                'relationship_id' => '10',
                'user_id' => '1',
            ],
        ]);

        DB::table('memories')->insert([
            [
                'title' => 'She said yes',
                'file_name' => '1 (11).jpg',
                'description' => 'Happiest day of my life',
                'trunk_id' => '3',
                'user_id' => '1',
                'type_id' => '1',
                'memory_date' => \Carbon\Carbon::createFromDate(2016,01,20),
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Finally',
                'file_name' => 'wedding11.jpg',
                'description' => 'So happy right now',
                'trunk_id' => '3',
                'user_id' => '1',
                'type_id' => '1',
                'memory_date' => '2016-03-14',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Latin document',
                'file_name' => 'older-document.pdf',
                'description' => 'Really interesting',
                'trunk_id' => '5',
                'user_id' => '1',
                'type_id' => '4',
                'memory_date' => '2000-03-14',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Other latin document ',
                'file_name' => 'older-document.docx',
                'description' => 'A must read',
                'trunk_id' => '5',
                'user_id' => '1',
                'type_id' => '4',
                'memory_date' => '2005-12-14',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Coding',
                'file_name' => 'desk-side.mp4',
                'description' => 'Having fun at work',
                'trunk_id' => '5',
                'user_id' => '1',
                'type_id' => '2',
                'memory_date' => '2009-12-14',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Old record',
                'file_name' => 'sound1.wav',
                'description' => 'Discovered it recently',
                'trunk_id' => '5',
                'user_id' => '1',
                'type_id' => '3',
                'memory_date' => '2015-12-14',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Nice ride',
                'file_name' => '203H.jpg',
                'description' => 'Found it in NY',
                'trunk_id' => '1',
                'user_id' => '1',
                'type_id' => '1',
                'memory_date' => '2012-11-14',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Cool bike',
                'file_name' => '245H.jpg',
                'description' => 'Found it on the way to NY',
                'trunk_id' => '1',
                'user_id' => '1',
                'type_id' => '1',
                'memory_date' => '2012-11-12',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Sunset',
                'file_name' => '478165662.jpg',
                'description' => 'Perfect spot for a picture',
                'trunk_id' => '4',
                'user_id' => '1',
                'type_id' => '1',
                'memory_date' => '2015-11-12',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'title' => 'Together',
                'file_name' => '479408986.jpg',
                'description' => 'All family at one place',
                'trunk_id' => '4',
                'user_id' => '1',
                'type_id' => '1',
                'memory_date' => '2012-11-19',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
        ]);

        DB::table('sharelinks')->insert([
            [
                'memory_id' => '1',
                'user_id' => '1',
                'hashed_link' => 'da19e8483968b2b25699d41cd93bb832'
            ],
            [
                'memory_id' => '5',
                'user_id' => '1',
                'hashed_link' => '60e9e46183d9f996049a2288d2fc633e'
            ],
        ]);

        DB::table('memory_person')->insert([
            [
                'memory_id' => '1',
                'person_id' => '1'
            ],
            [
                'memory_id' => '2',
                'person_id' => '1'
            ],
            [
                'memory_id' => '10',
                'person_id' => '1'
            ],
            [
                'memory_id' => '10',
                'person_id' => '2'
            ],
            [
                'memory_id' => '10',
                'person_id' => '3'
            ],
            [
                'memory_id' => '10',
                'person_id' => '4'
            ],
        ]);
        
    }
}