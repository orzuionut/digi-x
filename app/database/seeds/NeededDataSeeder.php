<?php

use Illuminate\Database\Seeder;

class NeededDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // memory types
        DB::table('memory_types')->insert([
            ['name' => 'image'],
            ['name' => 'video'],
            ['name' => 'audio'],
            ['name' => 'document'],
        ]);

        // memory types
        DB::table('relationship_types')->insert([
            [
                'name' => 'friend',
                'degree' => 5
            ],
            [
                'name' => 'father',
                'degree' => 1
            ],
            [
                'name' => 'mother',
                'degree' => 1
            ],
            [
                'name' => 'son',
                'degree' => 1
            ],
            [
                'name' => 'daughter',
                'degree' => 1
            ],
            [
                'name' => 'husband',
                'degree' => 1
            ],
            [
                'name' => 'wife',
                'degree' => 1
            ],
            [
                'name' => 'brother',
                'degree' => 2
            ],
            [
                'name' => 'sister',
                'degree' => 2
            ],
            [
                'name' => 'grandfather',
                'degree' => 2
            ],
            [
                'name' => 'grandmother',
                'degree' => 2
            ],
            [
                'name' => 'parent-in-law',
                'degree' => 2
            ],
            [
                'name' => 'son-in-law',
                'degree' => 2
            ],
            [
                'name' => 'daughter-in-law',
                'degree' => 2
            ],
            [
                'name' => 'uncle',
                'degree' => 3
            ],
            [
                'name' => 'aunt',
                'degree' => 3
            ],
            [
                'name' => 'nephew',
                'degree' => 3
            ],
            [
                'name' => 'niece',
                'degree' => 3
            ],
            [
                'name' => 'great grandchild',
                'degree' => 3
            ],
            [
                'name' => 'great grandparent',
                'degree' => 3
            ]
        ]);
    }
}
